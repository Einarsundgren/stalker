README ALL USERS
For more information please look at the developers section below, the wiki and at the project homepage http://einarsundgren.se/stalker/index.html
README DEVELOPERS

Stalker 1.0

*** General Client ***
There is only one package for this release. 
The se.einarsundgren.stalker contains all source classes needed to run and compile.


Change of orientation in an advanced way is disabled since reloading of the bitmaps
can cause a memory overflow on devices with low memory capacity.

This Android project is using OSMDroid, an open street map API for Android. 
To be able to compile and run you must install the OSM Droid libraries 
from  http://code.google.com/p/osmdroid/downloads/list to 
/Stalker/libs/osmdroid-android-3.0.10-javadoc.jar
/Stalker/libs/osmdroid-android-3.0.10.jar
/Stalker/libs/osmdroid-third-party-3.0.10.jar

*** API level ***
The project is in its main parts compatible with Android API-level 8-16. 
Full compatibility ranges 11-16 (Honey Comb and newer).
API-level 8-10 compiles but is not complete regarding functions 
and might cause runtime crashes so use with caution.

*** Server ***
The server is based on PHP 5.3 and Mysql that could be of basically any version.
This server is developed to be possible tu run on most semi recent LAMP-installations.
That said I will not go into details on how to set up such a combination but
assume you already have that knowledge.

To setup the server you need to accomplish two things. First a proper database. 
Following is the sql used create and extend the database during the development. 
It is constructed to be used by InnoDB engine but has mainly been tested on InnoDB engine. 
That said any of them should work equally well.

### Start of Stalker database creation
create database stalker;
use stalker;
create table if not exists stalkerUsers (userID int primary key auto_increment unique, password varchar(255), username varchar (40) unique, points int);
create table if not exists 
stalkerUserLocation(userID int, lat double, lon double, updated timestamp, foreign key (userID) references stalkerUsers(userID));
create table if not exists stalking (stalker int unique, stalked int, 
foreign key (stalker) references stalkerUsers (userID), foreign key (stalked) references stalkerUsers (userID));

## Some updates done during the development. 
ALTER TABLE stalking ADD (stalkedWarned TINYINT);
ALTER TABLE `stalking` ADD (interactionID BIGINT UNIQUE AUTO_INCREMENT);
ALTER TABLE stalking ADD (creationTime TIME_STAMP);
ALTER TABLE stalking ADD (hasBeenInrange TINYINT NOT NULL default 0);
### End of stalker database creation


When the database is created copy all the server files in server/src to the folder 
of your choice in the apaches php structure available to the public. Default is server.com/stalker

Edit the environmental variables in the file settings.php
Those should be database, host and user/password to the database.
The user you use must have full rights to read, write and delete the following tables
stalker, stalkerUsers, stalkerUserLocation

$database_name = 'stalker';
$database_user = '';
$database_password = '';
$database_host = '';

Now your server is ready.

The client should compile with these simple instructions. For detailed use of the client see the user manual.