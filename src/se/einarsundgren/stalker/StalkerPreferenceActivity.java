/*
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

import java.util.List;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;

/**
 * The preference activity called when the menu settings is selected.
 * This class contains a difference between pre and post Honeycomb systems
 * in that it uses fragmnet for the newer versions.
 * Created by Einar on 2013-06-23.
 * @author Einar Sundgren
 * @version 1.0
 */

public class StalkerPreferenceActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener{
	
	@SuppressWarnings("unused")
	private GameServer gameServer;
	@SuppressWarnings("unused")
	private Player currentPlayer;
	
	@Override
	public void onBuildHeaders(List<Header> target){
		super.onBuildHeaders(target);
		loadHeadersFromResource(R.xml.stalker_prefs_headers, target);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/* Backwards compatibility requires the use of deprecated methods*/
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			addPreferencesFromResource(R.xml.prefs);	
			
		}
		
		
		
		PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
		
		/* Use the shared Application objects*/
		this.gameServer = ((StalkerApplication) this.getApplication())
				.getGameServer();
		currentPlayer = ((StalkerApplication) this.getApplication())
				.getCurrentPlayer();
		
	}
	
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		Log.d("Prefs", "Change" + key);
		
		if (key.equals("PREF_GAME_SERVER_USER")||
				key.equals("PREF_GAME_SERVER_PASSWORD")||
				key.equals("GAME_SERVER_URI")){ 
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		
		Log.d("Prefs", "Updating player/password " + key);
		
		/* Needs to reload a lot of stuff and restart the service. basically restart the game with new info.*/
		
		/*Also make a controlled stop and restart of updater thread.*/
		Updater updater = ((StalkerApplication) getApplication()).getUpdater();
		synchronized (updater){
		updater.setRunFlag(false);
		updater.notify();
		}
		Intent intent = new Intent(getApplicationContext(), LoadActivity.class);
		startActivity(intent);
		updater.setRunFlag(true);
		updater.run();	
		}
	}
}
