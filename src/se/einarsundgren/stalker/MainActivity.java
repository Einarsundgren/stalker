/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
/***
 * This is the activity showing the main selection screen. 
 * This is the hub of the UI but contains no logic.
 * @author Einar Sundgren
 * @version 1.0
 *
 */
public class MainActivity extends Activity {
	private static final String TAG = "GameActivity";

	/**
	 *  This should be instantiated as local instances of some variables/objects in StalkerApplication
	 */
	
	/*
	 * They are used. Only Eclipse don�t understand that.
	 */
	@SuppressWarnings("unused")
	private Player currentPlayer;
	@SuppressWarnings("unused")
	private GameServer gameServer;


	/*
	 * Shared preferences object to allow access to all prefs
	 */
	SharedPreferences preferences;

	/*
	 * Dialog showing the first instruction.
	 */
	AlertDialog instructionsDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		/* Set Application level preferences to be the settings of this
		 acticvity.*/


		preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());

		/*
		 * Instatiatiing the shared objects
		 */
		gameServer = ((StalkerApplication) this.getApplication())
				.getGameServer();
		currentPlayer = ((StalkerApplication) this.getApplication())
				.getCurrentPlayer();

		
		if (preferences.getBoolean("PREF_SHOW_INSTRUCTIONS", true)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					MainActivity.this);
			
			LayoutInflater inflater = this.getLayoutInflater();
			View view = inflater.inflate(R.layout.dialog_instructions, null);
			builder.setView(view);
			
			final TextView message = (TextView) view.findViewById(R.id.instruction_m);
			message.setText(R.string.instruction_first);
			
			final AlertDialog dialog = builder.create();
			dialog.show();
			
			OnClickListener l = new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.cancel();
				}
			};
			Button okButton = (Button)view.findViewById(R.id.instructions_ok_button);
			okButton.setOnClickListener(l);			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/* Inflate the menu; this adds items to the action bar if it is present.*/
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent menuIntent;
		switch (item.getItemId()) {
		case R.id.action_settings:
			/* If settings selected, open settings activity.*/
			menuIntent = new Intent(this, StalkerPreferenceActivity.class);
			startActivityForResult(menuIntent, 1);
			return true;
		
		case R.id.to_about:
			menuIntent = new Intent(this, About.class);
			startActivityForResult(menuIntent, 1);
			return true;
			
		case R.id.to_logout:
			SharedPreferences.Editor editor = preferences.edit();

			editor.putString("PREF_GAME_SERVER_USER", null);
			editor.putString("PREF_GAME_SERVER_PASSWORD", null);

			//Select method depending on version. Apply is after api level 9
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD) editor.apply();
			else editor.commit();
			
			menuIntent = new Intent(this, LoadActivity.class);
			menuIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			menuIntent.putExtra("EXIT", true);
			startActivityForResult(menuIntent, 1);
			finish();
			return true;
		default:

			return false;
		}
	}


	/**
	 * Changes to StarteGameActivity
	 * @param view 
	 */
	public void startNewGame(View view) {
		Intent intent = new Intent(this, StartGameActivity.class);
		startActivity(intent);
	}

	/**
	 * Changes to GameInteraction Activity
	 * @param view
	 */
	public void gameInteraction(View view) {
		Intent intent = new Intent(this, GameInteractionActivity.class);
		startActivity(intent);
	}
	
	/**
	 * Changes to high Score activity
	 * @param view
	 */
	public void highScore(View view) {
		Intent intent = new Intent(this, HighScoreActivity.class);
		startActivity(intent);
	}
	
	public void playerInfo(View view) {
		Intent intent = new Intent(this, PlayerInfoActivity.class);
		startActivity(intent);
	}
	
	public void aboutGame(View view) {
		Intent intent = new Intent(this, About.class);
		startActivity(intent);
	}

	public void newPlayer(View view) {
		Intent intent = new Intent(this, NewPlayerActivity.class);
		startActivity(intent);
	}

}
