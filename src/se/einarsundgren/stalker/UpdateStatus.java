/*
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

public class UpdateStatus {
private int updatedItem;
private int status;

// Update Items
public static final int LOADED_PLAYER  = 1;
public static final int LOADED_POSITION  = 2;
public static final int LOADED_SERVER  = 3;
public static final int LOADED_NETWORK  = 4;

// Update statuses
public static final int SUCCESS = 1;
public static final int FAIL = 0;



public UpdateStatus(int updatedItem, int status) {
	super();
	this.updatedItem = updatedItem;
	this.status = status;
}
public int getUpdatedItem() {
	return updatedItem;
}
public void setUpdatedItem(int updatedItem) {
	this.updatedItem = updatedItem;
}
public int getStatus() {
	return status;
}
public void setStatus(int status) {
	this.status = status;
}
	
	
}
