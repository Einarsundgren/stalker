/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

//TODO: Create a shared timer so that the updates of each GUI is not making it query the server more than before. 
import java.util.ArrayList;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.util.Log;

/**
 * This is the shared application class. It contains no methods except for getters and setters.
 * Instead it provides a way to share date between all the different activities.
 * @author Einar Sundgren
 * @version 1.0
 * Created by Einar on 2013-06-20.
 */

public class StalkerApplication extends Application {
	private static final String TAG = "ApplicationActivity";
	// private static Object currentPlayer;
	
	private int serviceDelay = 5000;
	
	private boolean isStalked;

	private Updater updater;
	private Player currentPlayer;
	private static GameServer gameServer;
	private ArrayList<GameInteraction> gameInteractions;
	private ArrayList<Player> adjacentPlayers;
	
	// Preferences object
	SharedPreferences preferences;

	// Server URI
	private String serverName;
	private String userName;
	private String password;
	
	//Debug setting
	private boolean showStalkerCoordinates;
	
	private boolean isInteracting = false;
	private boolean isInGui = false;
	
	// Time and distance parameters
	public static final double OUT_OF_RANGE = 2;
	public static final double FAR = 1;
	public static final double NEAR = 0.5;
	public static final double ON_TOP = 0.2;
	
	public static final int FAR_SCORE = 2;
	public static final int NEAR_SCORE = 5;
	public static final int ON_TOP_SCORE = 10;
	public static final int BULLS_EYE_SCORE = 20;
	
	public static final int STALKING_TIME_LIMIT = 60*5;
	
	//Timings in micro seconds for update intervals.
	//When there is no interaction.
	public static final int LONG_SLEEP = 60000; 
	
	//When there is interaction.
	public static final int SHORT_SLEEP = 10000;
	public static final int MICRO_SLEEP = 3000;
	
	//GPS update values.
	public final static long MIN_LOCATION_UPDATE_TIME = 10000;
	public final static long MIN_LOCATION_UPDATE_DISTANCE = 10;
	
	
	// Conter on player or not.
	public final static boolean CENTER_ON_PLAYER = true;
	public final static boolean DONT_CENTER_ON_PLAYER = false;
	
	//Map settings
	public final static int INITIAL_ZOOM_LEVEL = 13;
	
	/***
	 *  Global updater interval to sync the different GUI:s and services.
	 *  Sets an update frequence that most server contacts must not be above.
	 */
	public long globalUpdateFreq = 3000;
	public Time lastUpdate;
	public boolean updating = false;

	@Override
	public void onCreate() {
		super.onCreate();

		preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());

		// gets settings from shared prefs
		serverName = preferences.getString("PREF_GAME_SERVER_URI", "Default");
		userName = preferences.getString("PREF_GAME_SERVER_USER", "Default");
		password = preferences.getString("PREF_GAME_SERVER_PASSWORD", "Default");
		showStalkerCoordinates = preferences.getBoolean("DEBUG_SHOW_STALKERS_EXACT", false);

	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public boolean isUpdating() {
		return updating;
	}

	public void setUpdating(boolean updating) {
		this.updating = updating;
	}

	public long getGlobalUpdate() {
		return globalUpdateFreq;
	}

	public void setGlobalUpdate(long globalUpdate) {
		this.globalUpdateFreq = globalUpdate;
	}

	public Time getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Time lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public void setCurrentPlayer(Player currentPlayer) {
		this.currentPlayer = currentPlayer;
		Log.d(TAG, "Currrent player set to: " + currentPlayer.getUserID());
	}

	public GameServer getGameServer() {
		return gameServer;
	}

	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}

	public ArrayList<GameInteraction> getGameInteractions() {
		return gameInteractions;
	}

	public void setGameInteractions(ArrayList<GameInteraction> gameInteractions) {
		this.gameInteractions = gameInteractions;
	}

	public boolean isInteracting() {
		return isInteracting;
	}

	public void setInteracting(boolean isInteracting) {
		this.isInteracting = isInteracting;
		Log.d("Application", "Interacting set to: " + isInteracting);
	}
	
	public int getServiceDelay() {
		return serviceDelay;
	}

	public void setServiceDelay(int serviceDelay) {
		Log.d("Application", "Sleep time changed to " + serviceDelay);
		this.serviceDelay = serviceDelay;
	}
	
	public Updater getUpdater() {
		Log.d(TAG, "Updater returned");
		return updater;
	}

	public void setUpdater(Updater updater) {
		this.updater = updater;
		Log.d(TAG, "Updater set");
	}
	
	//TODO: Rewrite. Suggestion from SO
	/*
	 * NB! This code is not mine. It is completely and shamelessly borrowed from
	 * a Stackoverflow thread.
	 * http://stackoverflow.com/questions/4238921/android-detect-whether-there-is-an-internet-connection-available
	 * I make no claims that this is mine. As it turns out I dont even make claims that this works 
	 * in the way I expected. Hence the TODO before the comment.
	 */
	public static boolean isNetworkAvailable(Context context) {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public boolean isStalked() {
		return isStalked;
	}

	public void setStalked(boolean isStalked) {
		this.isStalked = isStalked;
	}

	public boolean isInGui() {
		return isInGui;
	}

	public void setInGui(boolean isInGui) {
		this.isInGui = isInGui;
	}

	public ArrayList<Player> getAdjacentPlayers() {
		return adjacentPlayers;
	}

	public void setAdjacentPlayers(ArrayList<Player> adjacentPlayers) {
		this.adjacentPlayers = adjacentPlayers;
	}


}
