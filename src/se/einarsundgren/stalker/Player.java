/*
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.einarsundgren.stalker;

import java.util.Date;

/**
 * Models a player in the game. It could be either the current player, a stalker, a stalked or another 
 * unrealated player. Any relations to other player objects are not considered only information taht 
 * concerns the player no matter what such as position, points, name etc.
 * Created by Einar on 2013-06-16.
 */
public class Player {
	private double lat;
	private double lon;
	private String password = null;
	private String userName = null;
	private int userID;
	private int points;
	private Date lastUpdated;

	public Player(String userName, int userID, String password, double lat,
			double lon, int points) {
		this.lat = lat;
		this.lon = lon;
		this.password = password;
		this.userName = userName;
		this.userID = userID;
		this.points = points;
	}

	public Player(String userName, String password) {
		this.userName = userName;
		this.password = password;}

	public Player(String userName, int points) {
			this.userName = userName;
			this.points = points;
	}
	
	public Player(int userID){
		this.userID=userID;
	};

	public double getLat() {
		return lat;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public Player() {

	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
