/*
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.einarsundgren.stalker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
/**
 * UI for adding a new player. 
 * Just the basic structure of it. 
 * @author Einar Sundgren
 * @version 0.1
 *
 */
public class NewPlayerActivity extends Activity {
	protected TextView usernameComment;
	protected TextView emailComment;
	protected TextView passwordComment ;
	protected EditText newUserName ; 
	protected EditText newUserPassword ;
	protected EditText newUserEmail ;
	protected Button acceptButton;
	
	protected GameServer gameServer;
	
	protected boolean passwordOk = false;
	protected boolean nameOk = false;
	protected boolean mailOk = false;
	
	private SharedPreferences preferences;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		this.gameServer = ((StalkerApplication) this.getApplication())
				.getGameServer();
		preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		
		setContentView(R.layout.activity_new_player);
		usernameComment = (TextView) findViewById(R.id.usernameComment);
		emailComment = (TextView) findViewById(R.id.emailComment);
		passwordComment = (TextView) findViewById(R.id.passwordComment);
		newUserName = (EditText) findViewById(R.id.new_user_name); 
		newUserPassword = (EditText) findViewById(R.id.new_user_password);
		newUserEmail = (EditText) findViewById(R.id.new_user_email);
		acceptButton = (Button) findViewById(R.id.createPlayerButton);
		
		usernameComment.setAlpha(0);
		passwordComment.setAlpha(0);
		emailComment.setAlpha(0);
		
		toggleButton();
		
	acceptButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Log.d("Registering", "Registering");
			new RegisterAsync().execute("");
			//registerNewPlayer();
			
		}
	});	
		
	// Creates the text listeners to the text fields	
	TextWatcher userNameWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			
			
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			CheckExistingAsync checkExistingAsync = new CheckExistingAsync();
			 checkExistingAsync.execute("");			
		}
	};
	
	TextWatcher userPasswordWatcher = new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			String password = newUserPassword.getText().toString();
			if (password.length()<6){
				passwordComment.setAlpha(100);
				passwordComment.setText(R.string.passwordTooShort);
				passwordComment.setTextColor(Color.RED);
				passwordOk = false; 
				toggleButton();
			}else if (password.matches("[a-zA-Z]+")){
				passwordComment.setText(R.string.passwordBad);
				passwordComment.setTextColor(Color.RED);
				passwordOk = false;
				toggleButton();
			} else {
				passwordComment.setText(R.string.passwordOk);
				passwordComment.setTextColor(Color.GREEN);
				passwordOk = true;
				toggleButton();
			}	
		}
	};
	
	
	
	TextWatcher userEmailWatcher = new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			
			
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			String email = newUserEmail.getText().toString();

			String regex = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
			String mailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			
			//Log.d("Mail", "Email: " + email);
			if (email.matches(mailPattern)){
				emailComment.setAlpha(100);
				emailComment.setText(R.string.emailOk);
				emailComment.setTextColor(Color.GREEN);
				mailOk = true;
				toggleButton();
				
			}else {
				emailComment.setAlpha(100);
				emailComment.setText(R.string.emailInvalid);
				emailComment.setTextColor(Color.RED);
				mailOk = false;
				toggleButton();
				
			}			
		}
	};
	
	
	
	newUserEmail.addTextChangedListener(userEmailWatcher);
	newUserPassword.addTextChangedListener(userPasswordWatcher);
	newUserName.addTextChangedListener(userNameWatcher);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_player, menu);
		return true;
	}

	
	
	private class CheckExistingAsync extends AsyncTask<String, Integer, Boolean> {
		String userName =  newUserName.getText().toString();
		@Override
		protected Boolean doInBackground(String... params) {
			publishProgress(1);
			Log.d("Test", "calling async");
			return gameServer.playerNameExists(userName);
		}
		@Override
		protected void onProgressUpdate(Integer...progress){
			usernameComment.setAlpha(100);
			usernameComment.setTextColor(Color.RED);
			usernameComment.setText(R.string.userNameCheck);
		}
		
		@Override
		protected void onPostExecute(Boolean params){
			
			Log.d("Test", userName);
			if (userName.length()<5){
				usernameComment.setAlpha(100);
				usernameComment.setTextColor(Color.RED);
				usernameComment.setText(R.string.userNameTooShort);
				nameOk = false;
				toggleButton();
			} else if (params){
				usernameComment.setAlpha(100);
				usernameComment.setTextColor(Color.RED);
				usernameComment.setText(R.string.userNameExists);
				nameOk = false;
				toggleButton();
			} 
			else{
				usernameComment.setText(R.string.userNameGood);
				usernameComment.setTextColor(Color.GREEN);
				nameOk = true;
				toggleButton();
			}
			
		}
	};
	
	/**
	 * Toggles button on or off depending on values in inputboxes
	 */
	protected void toggleButton(){
		if (nameOk&&passwordOk&&mailOk) acceptButton.setEnabled(true); 
		else acceptButton.setEnabled(false);
	}
	
	protected boolean registerNewPlayer(){
		
		try {
			Log.d("Register", "Calling gameserver");
			return gameServer.registerNewPlayer(newUserName.getText().toString(), newUserPassword.getText().toString(), newUserEmail.getText().toString());
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d("Register", "Returning false.");
		return false;
	}
	
	private class RegisterAsync extends AsyncTask<String, Integer, Integer>{

		@Override
		protected Integer doInBackground(String... params) {
			if (!registerNewPlayer()){
			Log.d("Register", "Insertion failed. Return false");
				cancel(true);  // Cancels if return is false.
			}
			return null;
		}
		@Override 
		protected void onCancelled(){
			Log.d("Register", "cancelled");
			
			Toast.makeText(getApplicationContext(), R.string.failed_to_create_user, Toast.LENGTH_LONG).show();

		}
		
		@Override
		protected void onPostExecute(Integer i){
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("PREF_GAME_SERVER_USER", newUserName.getText().toString());
			editor.putString("PREF_GAME_SERVER_PASSWORD", newUserPassword.getText().toString());
			editor.apply();
			Intent intent = new Intent(getApplicationContext(), LoadActivity.class);
			startActivity(intent);
			finish();
		}
	
	}
}
