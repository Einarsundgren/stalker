/*
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A toast that shows how the score changes. When it changes.
 * @author Einar Sundgren
 * @version 1.0
 *
 */
public class ScoreDifferenceToast implements Runnable {

	private Context context;
	private int difference;
	

	    public ScoreDifferenceToast(Context context, int difference) {
		super();
		this.context = context;
		this.difference = difference;
		
		
	}


		public void run() {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
	    	
			View layout = inflater.inflate(R.layout.toast_scorechange,null);
			TextView text = (TextView)layout.findViewById(R.id.score_chaneg_text);
			Drawable upArrow = layout.getResources().getDrawable(R.drawable.arrow_up);
			Drawable downArrow = layout.getResources().getDrawable(R.drawable.arrow_down);
			
			


			Toast toast = new Toast(context.getApplicationContext());
			toast.setGravity(Gravity.BOTTOM, 0, 0);
			toast.setDuration(Toast.LENGTH_SHORT);
			if (difference==0){
			layout.setBackgroundColor(context.getResources().getColor(R.color.blue_score_neutral));
			upArrow.setAlpha(0);
			downArrow.setAlpha(0);
			text.setText("Your score was unchanged.");}
			else if (difference <0) {
				upArrow.setAlpha(0);
				downArrow.setAlpha(1);
				layout.setBackgroundColor(context.getResources().getColor(R.color.red_score_down));
				text.setText("You lost " + Math.abs(difference) +"points");
				
			}
			
			else if (difference >0) {
			layout.setBackgroundColor(context.getResources().getColor(R.color.green_score_up));
			upArrow.setAlpha(1);
			downArrow.setAlpha(0);
			text.setText("You gained "+difference +" points.");	
			}
			
			
			toast.setView(layout);
			toast.show();
			
			
			/*Toast toast = Toast.makeText(this.context,
					"Your score was changed with " + difference + " points.", 
					Toast.LENGTH_SHORT);*/
			//if (difference==0) toast.
	    	
	    	//toast.show();
	    }
	 }
	

