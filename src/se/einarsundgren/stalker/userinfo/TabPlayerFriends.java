package se.einarsundgren.stalker.userinfo;

import java.util.ArrayList;

import se.einarsundgren.stalker.GameServer;
import se.einarsundgren.stalker.Player;
import se.einarsundgren.stalker.R;
import se.einarsundgren.stalker.StalkerApplication;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.TextView;
import android.widget.Toast;

public class TabPlayerFriends extends Fragment{
	Player currentPlayer;
	GameServer gameServer;
	ArrayList<Player> friends = new ArrayList<Player>();
	TextView text;
	View view;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		  
		       // super.onCreate(savedInstanceState);
		        view = inflater.inflate(R.layout.tab_player_friends, null);
		        text = (TextView) view.findViewById(R.id.player_friends_text);
		        new AddFriendAsync().execute(3,7);
		        return view;
	  }

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
        currentPlayer = ((StalkerApplication) getActivity().getApplication())
				.getCurrentPlayer();
        gameServer = ((StalkerApplication) getActivity().getApplication())
				.getGameServer();
		
		
		
	}
	
private class AsyncGetFriends extends AsyncTask<String,String,String>{

	@Override
	protected String doInBackground(String... arg0) {
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result){
		text.setText("Did run it");
	}
	
}

public class AddFriendAsync extends AsyncTask<Integer, Integer, Boolean>{

	
	
	@Override
	protected Boolean doInBackground(Integer... players) {
		Boolean result = gameServer.addFriend(players[0], players[1]);
		friends = gameServer.getFriends(currentPlayer);
		return result;
	}
	
	@Override
	protected void onPostExecute(Boolean result){
		if (result) {

		}
		Toast toast = Toast.makeText(getActivity(), "Friend added", Toast.LENGTH_LONG);
		toast.show();
		
		for (int i = 0; i<friends.size(); i++){
		text.append(friends.get(i).getUserName()+"\n");
		}
		
		
	}
			

}



}
