package se.einarsundgren.stalker.userinfo;

import se.einarsundgren.stalker.Player;
import se.einarsundgren.stalker.R;
import se.einarsundgren.stalker.StalkerApplication;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TabPlayerDetails extends Fragment{
	Player currentPlayer;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		  
		        super.onCreate(savedInstanceState);
		        View view = inflater.inflate(R.layout.tab_player_details, null);
		        currentPlayer = ((StalkerApplication) getActivity().getApplication())
						.getCurrentPlayer();
		        
		       TextView text = (TextView) view.findViewById(R.id.player_details_text);

		        text.setText("Player: " + currentPlayer.getUserName() +" with ID" + currentPlayer.getUserID() +" and points "+ currentPlayer.getPoints() 
						 +" last recorded pos: Lat " + currentPlayer.getLat() +" Lon " + currentPlayer.getLon());
		        		        
		        return view;
	  }
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}
}
