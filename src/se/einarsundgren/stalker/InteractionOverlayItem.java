/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;


/**
 * A class extending overlayItem to make it possible to pass GameInteraction objects as well
 * @author Einar Sundgren
 * @version 1.0
 *
 */

public class InteractionOverlayItem extends OverlayItem {
	//final GameInteractionActivity outer;
	
	public GameInteraction gameInteraction;
	Player player;

	//Just a new constructor helping to pass an interaaction object to the overlay.
	public InteractionOverlayItem(GeoPoint aGeoPoint,
			GameInteraction gameInteraction, String message) {
		super(gameInteraction.getStalker().getUserName(),
				message, aGeoPoint);
		this.gameInteraction = gameInteraction;

	}
	
	// An overload so also a player object can be passed.
	public InteractionOverlayItem(GeoPoint aGeoPoint,
			Player player, String message) {
		super(player.getUserName(),	message + "lat: " + player.getLat() +" lon: " + player.getLon(), aGeoPoint);
		this.player = player;
	}
	
	
}