/*
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.einarsundgren.stalker;

import se.einarsundgren.stalker.userinfo.TabListener;
import se.einarsundgren.stalker.userinfo.TabPlayerAchievments;
import se.einarsundgren.stalker.userinfo.TabPlayerDetails;
import se.einarsundgren.stalker.userinfo.TabPlayerFriends;
import android.os.Bundle;
import android.app.ActionBar;
import android.content.res.Resources;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;

/**
 * 
 * Displays info on the current playing player.
 * @author Einar Sundgren
 * @version 1.0
 */
public class PlayerInfoActivity extends FragmentActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player_info);
		
	    final ActionBar actionBar = getActionBar();
	    
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	    // For each of the sections in the app, add a tab to the action bar.
	    actionBar.addTab(actionBar.newTab().setIcon(getResources().getDrawable(R.drawable.arrow_up))
	        .setTabListener(new TabListener<TabPlayerDetails>(this, "Details", TabPlayerDetails.class)));
	    actionBar.addTab(actionBar.newTab().setIcon(getResources().getDrawable(R.drawable.arrow_up))
	        .setTabListener(new TabListener<TabPlayerAchievments>(this, "Achievments", TabPlayerAchievments.class)));
	    actionBar.addTab(actionBar.newTab().setIcon(getResources().getDrawable(R.drawable.arrow_up))
	        .setTabListener(new TabListener<TabPlayerFriends>(this, "Friends", TabPlayerFriends.class)));	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.player_info, menu);
		return true;
	}



}
 