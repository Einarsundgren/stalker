/*
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;



//TODO: Trim location manager to make use of the best location provider.//TODO: Rename this service to something more revealing

import java.util.ArrayList;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 *
 * Created by Einar Sundgren on 2013-06-15.
 * This class is a service that updates the users current location on a set interval . 
 * If it fails to do that the error is handled by the called method
 * saveCoordinates. The thread will keep going.
 * 
 * This could not be refactored into Updater class since the class using any
 * location service must extend Service or Activity. This is a way of ircling that.
 * 
 * @author Einar Sundgren
 * @version 1.0
 */
public class SetLocation extends Service {
	
	private static final String TAG = "UpdaterService";
	
	protected boolean runFlag = true;
	protected boolean isInteracting = false;
	

	/* Get the name of the provider*/
	String locationProvider = LocationManager.GPS_PROVIDER;
	LocationManager locationManager;

	Context context;

	/* Local instances of the shared object. Initialized later.*/
	protected Updater updater;
	protected Player currentPlayer;
	protected GameServer gameServer;
	protected ArrayList<GameInteraction> gameInteractions;
	protected GameInteraction currentInteraction;
	protected ArrayList<Player> adjacentPlayers;
	
	/**
	 * An object of the instance of this class to be able to pass data from threads in this class. 
	 */
	SetLocation currentSetLocation  = this;
	
	String serverName;

	public void warnForNetworkError() {
		Context context = getApplicationContext();
		Toast toast;
		toast = Toast.makeText(context, R.string.network_failed_toast,
				Toast.LENGTH_LONG);
		toast.show();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();

		PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());

		/* Acquire a reference to the system Location Manager*/
		locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		
		/* Register the listener with the Location Manager to receive location
		 updates. Se finals in Application Activity for details.*/
		locationManager.requestLocationUpdates(locationProvider, 
				StalkerApplication.MIN_LOCATION_UPDATE_TIME, 
				StalkerApplication.MIN_LOCATION_UPDATE_DISTANCE,
				locationListener);

		Log.d(TAG, "Location manager created");
		

		/* Use the shared player object */
		currentPlayer  = ((StalkerApplication) currentSetLocation.getApplication()).getCurrentPlayer();
		
		/* Use shared server object*/
		gameServer = ((StalkerApplication) currentSetLocation.getApplication()).getGameServer();

		/* Use shared interactions object */
		gameInteractions = ((StalkerApplication) currentSetLocation.getApplication()).getGameInteractions();
		
		/* Use the shared flag for interaction mode */
		isInteracting  = ((StalkerApplication) currentSetLocation.getApplication()).isInteracting();

		/* Use the shared updater objects*/
		updater  = ((StalkerApplication) currentSetLocation.getApplication()).getUpdater();
		
		/* Shared delay for time between server contacts.
		 Create a new object of the main logic class and start it.*/
		/***
		 * Here the game starts.
		 */
		updater = new Updater(this);
		updater.start();
		
		((StalkerApplication) currentSetLocation.getApplication()).setUpdater(updater);
				
		Log.d(TAG, "Game logic and updater thread started.");
	}
	
	/* Define a listener that responds to location updates*/
	LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location) {
			Log.d(TAG, "locations changed");
			if (currentPlayer!=null){
			currentPlayer.setLat(location.getLatitude());
			currentPlayer.setLon(location.getLongitude());
			Log.d(TAG, "lat: "+ location.getLatitude());
			Log.d(TAG, "long: "+ location.getLongitude());
			}

		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onProviderDisabled(String provider) {
		}
	};

	public IBinder onBind(Intent intent) {
		return null;
	}

	
	
}