/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

/***
 * This thread is the main logic thread of the game started onCreate of an outer service. 
 * This updates the shared application object and is controlled by the UI activities 
 * by setting flags in the application object.
 * 
 * @author Einar Sundgren
 * @version 1.0
 *
 */
class Updater extends Thread {
	Handler handler = new Handler();
	final SetLocation outer;
	final String TAG = "Updater";
	private boolean isStalked = false;
	private boolean wasStalked = false;
	private Location lastKnownLocation;
	private boolean runFlag = true;
	private boolean firstRun = true;

	//TODO: Set a flag so updater don�t start a new thread before the first thread is finished.
	/* One thread per updater class should be the rule.*/
	public Updater(SetLocation outer) {
		this.outer = outer;
	}
	
	SetLocation mSetLocation = this.outer;

	@Override
	public void run() {
		if (firstRun){
		/* Register with the Android looper if its the first turn it runs...*/
		Looper.prepare();
		firstRun = false;
		}
		Log.d(TAG, "Started to run game logic thread");
		
		while (runFlag) {
		Log.d(TAG, "Runner loop started.");
			//Looper.loop();	// Commented out this since it seemed to halt the program.

			/***
			 *  Update the shared game objects
			 */
			Log.d(TAG, "Set currentplayer.");
			outer.currentPlayer  = ((StalkerApplication) outer.currentSetLocation.getApplication()).getCurrentPlayer();
			Log.d(TAG, "Set server.");
			outer.gameServer = ((StalkerApplication) outer.currentSetLocation.getApplication()).getGameServer();
			Log.d(TAG, "Set interactions.");
			outer.gameInteractions = ((StalkerApplication) outer.currentSetLocation.getApplication()).getGameInteractions();
			Log.d(TAG, "Set is interacting.");
			outer.isInteracting  = ((StalkerApplication) outer.currentSetLocation.getApplication()).isInteracting();
			Log.d(TAG, "Set isStalked.");
			isStalked = ((StalkerApplication) outer.currentSetLocation.getApplication()).isStalked();
			Log.d(TAG, "Set adjacent.");
			outer.adjacentPlayers  = ((StalkerApplication) outer.currentSetLocation.getApplication()).getAdjacentPlayers();

			
			// isNetworkAvailable()
			/* Checks if online. Skips updating if not online.
			 CHECK NOT IN USE FOR NOW*/
			//if (true) {
			Log.d(TAG, "Trying to get last know location.");
				try {
					
					lastKnownLocation = outer.locationManager
							.getLastKnownLocation(outer.locationProvider);
					Log.d(TAG, "Got last known location.");

					/*
					 *  Check if the location is at all set. This is for updating the location.
					 */
					if (lastKnownLocation != null) {
						Log.d(TAG, "Updater fires");
						
						/*
						 *  Save location to the player object
						 */
						outer.currentPlayer.setLat(lastKnownLocation.getLatitude());
						outer.currentPlayer.setLon(lastKnownLocation.getLongitude());
						
						Log.d(TAG, Double.toString(lastKnownLocation.getLatitude()));
						Log.d(TAG, Double.toString(lastKnownLocation.getLongitude()));
						
						/* Check if server returns 11, then it had success with both operations.*/
						if (outer.gameServer.saveCoordinates(outer.currentPlayer)
								.equals("11")) {
							Log.d(TAG,
									"Server received and answered the update call");
						} else
							Log.d(TAG, "Error in server connection");
					} else
						Log.d(TAG, "Location null");
				} catch (Exception e){
					Log.d(TAG, "Error updating location: " + e.getMessage());
				}
				
				
				/*Getting adjacent players.*/
				try{
					outer.gameServer.setNumPlayers(10);
					Log.d(TAG,"Getting adjacent players for" + outer.currentPlayer.getUserName());
					outer.adjacentPlayers = outer.gameServer.listPossibleVictims(outer.currentPlayer);	
					((StalkerApplication) outer.currentSetLocation.getApplication()).setAdjacentPlayers(outer.adjacentPlayers);
				}catch (Exception e) {
					Log.d(TAG, "Error getting adjacent players: " + e.getMessage());
				}
					
					
				try{
					/**
					 * Sets the shared interactions object to one retreived from the server.
					 */
					Log.d(TAG,"Getting interactions");
					outer.gameInteractions = outer.gameServer.getGameInteractions(outer.currentPlayer);
					
					((StalkerApplication) outer.currentSetLocation.getApplication())
					.setGameInteractions(outer.gameInteractions);
				}catch (Exception e){
					Log.d(TAG, " Error getting interactions: " + e.getMessage());
				}
					
					/**
					 * Go through each of the interactions. 
					 * check each if it gives this user plus or minus.
					 * Sum the points. Send it to the server.
					 * If connection failed, save the points and send them later.
					 */
					
				try{
					int difference = 0;
					/* Iterate over all Interaction items retreived.*/
					
					/* Get out of interaction mode as default state.*/
					outer.isInteracting = false;
					((StalkerApplication) outer.currentSetLocation.getApplication()).setInteracting(outer.isInteracting);
					
					/* Has the server returned any interactions at all?*/
					if (outer.gameInteractions != null){
					
					Log.d(TAG, "size: " + outer.gameInteractions.size());

					for (int i = 0; i < outer.gameInteractions.size(); i++) {
						/* Get into interaction mode both locally and globally */
						outer.isInteracting = true;
						((StalkerApplication) outer.currentSetLocation.getApplication()).setInteracting(outer.isInteracting);
						
						outer.currentInteraction = outer.gameInteractions.get(i);
						
						/*Log age of interaction*/
						Log.d(TAG, "Age: " + outer.currentInteraction.getAge());
						
						/*If time diff is > 1 minutes. Stop interaction.*/
						if (outer.currentInteraction.getAge() > StalkerApplication.STALKING_TIME_LIMIT) {
							Log.d(TAG, "Stop stalking bco age");
							outer.gameServer.stopStalking(outer.currentInteraction.getStalker(), outer.currentInteraction.getStalked());
							handler.post(new GamePlayNotification(outer.context, 
									outer.getString(R.string.game_ending_time_title), 
									outer.getString(R.string.game_ending_time_text)));
						}
						
						/* Reset flags for this player beeing stalked.*/
						isStalked = true;
						((StalkerApplication) outer.currentSetLocation.getApplication()).setStalked(isStalked);
						/* Check if current player is stalked in this interaction*/
						if (outer.currentInteraction.getStalked().getUserID() == outer.currentPlayer.getUserID()) {
							Log.d(TAG, "Looses");
							
							//TODO: Make a check if user was stalked before. If so, send notification that user is not stalked anymore
							wasStalked = true;
							isStalked = true;
							((StalkerApplication) outer.currentSetLocation.getApplication()).setStalked(isStalked);
							
							/* If the players has been in range and now are out of range stop game*/
							if (outer.currentInteraction.getDistance()>=StalkerApplication.OUT_OF_RANGE && 
									outer.currentInteraction.hasBeenInRange()){
								/*Somehow the users got out of range of each other. Game stopped.*/
								outer.gameServer.stopStalking(outer.currentInteraction.getStalker(), outer.currentInteraction.getStalked());

								handler.post(new GamePlayNotification(outer.context, 
										outer.getString(R.string.game_ending_escaped_title), 
										outer.getString(R.string.game_ending_escaped_text_stalker)));
								}
							
							/* If player is within rage and flag for that not has been set, set flag.*/
							if (outer.currentInteraction.getDistance()<StalkerApplication.OUT_OF_RANGE && 
									!outer.currentInteraction.hasBeenInRange()){
								outer.gameServer.setHasBeenInRange(outer.currentInteraction);
							}
							
							
							/*Adjust the score from the distance*/
							if  (outer.currentInteraction.getDistance()<StalkerApplication.OUT_OF_RANGE && 
									outer.currentInteraction.getDistance()>=StalkerApplication.FAR
									){ difference -=StalkerApplication.FAR_SCORE;
							   			Log.d(TAG, "+ Points for far");}
							
							else if (outer.currentInteraction.getDistance()<StalkerApplication.FAR&&
									outer.currentInteraction.getDistance()>=StalkerApplication.NEAR
									){ difference -=StalkerApplication.NEAR_SCORE;
							Log.d(TAG, "+ Points for near");
							}
							
							else if (outer.currentInteraction.getDistance()<StalkerApplication.NEAR&&
									outer.currentInteraction.getDistance()>=StalkerApplication.ON_TOP){ 
								difference -=StalkerApplication.ON_TOP_SCORE;
							Log.d(TAG, "+ Points for on top");
							}
							else if (outer.currentInteraction.getDistance()<StalkerApplication.ON_TOP) {
								difference -=StalkerApplication.BULLS_EYE_SCORE;
							Log.d(TAG, "+ Points for bulls eye");
							}
							
							/* Has the stalked player been notified? If not notify.*/
							if (!outer.currentInteraction.isStalkedWarned()){
								Log.d(TAG, "You should be warned! But then Im turning the warning off");
								outer.gameServer.setStalkedWarned(outer.currentInteraction);
								
								/* Handler to run things in GUI thread.
								 Notify about a player stalking current player*/
								String title = outer.getString(R.string.warn_stalked_title);
								String text = String.format(outer.getString(R.string.warn_stalked_text), outer.currentInteraction.getStalker());
								handler.post(new GamePlayNotification(outer.context, title, text));

								
							} else
							Log.d(TAG, "You should not be warned!" + outer.currentInteraction.isStalkedWarned());
							
						}
						
						/* Not stalked, should be stalker then. 
						  Otherwise smthng is really wrong and
						 an exception should be thrown.*/
						else if (outer.currentInteraction.getStalker().getUserID() == outer.currentPlayer
								.getUserID()) {
							/*Adjust score calculator*/
							
				        /* If the players has been ni range and now are out of range stop game*/
							if (outer.currentInteraction.getDistance()>=StalkerApplication.OUT_OF_RANGE && 
									outer.currentInteraction.hasBeenInRange()){
								/*Somehow the users got out of range of each other. Game stopped.*/
								Log.d(TAG, "Stop stalking bc out of range 2");
								outer.gameServer.stopStalking(outer.currentInteraction.getStalker(), outer.currentInteraction.getStalked());
								difference+=0;
								handler.post(new GamePlayNotification(outer.context, 
										outer.getString(R.string.game_ending_escaped_title), 
										outer.getString(R.string.game_ending_escaped_text_stalker)));

							}
							
							/* If player is within rage and flag for that not has been set, set flag.*/
							if (outer.currentInteraction.getDistance()<StalkerApplication.OUT_OF_RANGE && 
									!outer.currentInteraction.hasBeenInRange()){
								outer.gameServer.setHasBeenInRange(outer.currentInteraction);
							}

							/*Adjust the score from the distance */
							if  (outer.currentInteraction.getDistance()<StalkerApplication.OUT_OF_RANGE && 
									outer.currentInteraction.getDistance()>=StalkerApplication.FAR
									){ difference +=StalkerApplication.FAR_SCORE;
							   			Log.d(TAG, "+ Points for far");}
							
							else if (outer.currentInteraction.getDistance()<StalkerApplication.FAR&&
									outer.currentInteraction.getDistance()>=StalkerApplication.NEAR
									){ difference +=StalkerApplication.NEAR_SCORE;
							Log.d(TAG, "+ Points for near");
							}
							
							else if (outer.currentInteraction.getDistance()<StalkerApplication.NEAR&&
									outer.currentInteraction.getDistance()>=StalkerApplication.ON_TOP){ 
								difference +=StalkerApplication.ON_TOP_SCORE;
							Log.d(TAG, "+ Points for on top");
							}
							else if (outer.currentInteraction.getDistance()<StalkerApplication.ON_TOP) {
								difference +=StalkerApplication.BULLS_EYE_SCORE;
							Log.d(TAG, "+ Points for bulls eye");
							}
							
							Log.d(TAG, "Gains");
						}
						
					}
				/*Update server with new score*/
					outer.gameServer.changeScore(outer.currentPlayer, difference);
					ScoreDifferenceToast sdToast = new ScoreDifferenceToast(outer.context,difference);
					handler.post(sdToast);
				} 
				
					/* If user was stalked and now isn�t any more. Send a notification.*/
					if (wasStalked &&!isStalked) {
					Log.d(TAG, "Stalking of you ended.");

					handler.post(new GamePlayNotification(outer, 
							outer.getString(R.string.game_ending_stalked_title), 
							outer.getString(R.string.game_ending_stalked_text)));
				wasStalked = false;	
				}
					
					/*Adjust sleep delay depending on mode.*/
					if (((StalkerApplication) outer.currentSetLocation.getApplication()).isInteracting()
							|| ((StalkerApplication) outer.currentSetLocation.getApplication()).isInGui()
							) 
						{
						Log.d(TAG, "Micro sleep");
						((StalkerApplication) outer.currentSetLocation.getApplication()).setServiceDelay(StalkerApplication.MICRO_SLEEP);
						}
					else 
						{
						Log.d(TAG, "Long sleep");
						((StalkerApplication) outer.currentSetLocation.getApplication()).setServiceDelay(StalkerApplication.LONG_SLEEP);
						}	
				} 
				catch (Exception e) {
					Log.d(TAG, "Error, generic exception: " + e.getMessage());
				}
				
				/*Sleep thread stipulated time.*/
				try {
					
					synchronized (this) {
						this.wait(((StalkerApplication) outer.currentSetLocation.getApplication()).getServiceDelay());	
					}
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
			//} End of if true
			
		}// End of while loop
		Log.d(TAG, "Updater stopped. Runflag set to false.");
		Looper.myLooper().quit();
	}

	public boolean isRunFlag() {
		return runFlag;
	}

	public void setRunFlag(boolean runFlag) {
		this.runFlag = runFlag;
	}
	
	
}