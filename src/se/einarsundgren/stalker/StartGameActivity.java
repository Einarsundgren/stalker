/*
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;


import java.util.ArrayList;
import java.util.List;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;

import se.einarsundgren.stalker.R.layout;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.ClipData.Item;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;


/***
 * This is the UI class for a player to start a new game. 
 * It also launches a runnable that updates the UI on a set interval.
 * Created by Einar Sundgrenon 6/17/13.
 * @author Einar Sundgren
 * @version 1.0
 * 
 * 
 */
public class StartGameActivity extends Activity {
	
	private final String TAG = "StartGameActivity";
	/* Shared player object*/
	private Player currentPlayer;

	@SuppressWarnings("unused") /* Is actually used...*/
	private ArrayList<GameInteraction> gameInteractions;
	
	/* Create prefs*/
	SharedPreferences preferences;

	private Handler handler = new Handler();

	private Runnable runnable =  new Runnable() {
		   @Override
		   public void run() {
		      /* Things to do on interval.
			    Refresh GUI but don't center on player.*/
			   Log.d(TAG, "Updating start game UI" );
				new GetVictimsAsync(false).execute(currentPlayer);
			   /* Call itself again with the same interval.*/
		      handler.postDelayed(this, StalkerApplication.MICRO_SLEEP);
		   }
		};
		
		Button closeButton;
		Button stalkButton;
		Button friendButton;
		TextView victimInfo;
	private GetVictimsAsync getVictimsAsync = new GetVictimsAsync(true);
	
	protected VictimsOverlay victimsOverlay;
	protected ItemizedOverlayWithFocus<OverlayItem> playerOverlay;
	protected ArrayList<OverlayItem> playerItems = new ArrayList<OverlayItem>();
	protected ArrayList<VictimsOverlayItem> victimItems = new ArrayList<VictimsOverlayItem>();

	private ResourceProxy resourceProxy;

	private MapView mapView;
	private MapController mapController;
	
	private Updater updater;
	
	protected StartGameActivity outer = this;
	
	protected TextView instructionsView;
	
	protected PopupWindow pwindo;
	protected int touchX, touchY;
	
	protected VictimsOverlayItem currentItem;

	/*
	 * Load the bitmaps and drawables
	 */
	protected Bitmap youBitmap;
	protected BitmapDrawable youBmDrawable;
	
	Bitmap victimBitmap;
	BitmapDrawable victimBitapBitmapDrawable;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_start_game);
		
		/***
		 * Setting up some of the shared objects.
		 */
		
		/* Set Application level preferences to be the settings of this
		 acticvity.*/
		preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		// Use the shared Application objects
		currentPlayer = ((StalkerApplication) this.getApplication())
				.getCurrentPlayer();
		gameInteractions = ((StalkerApplication) this.getApplication())
				.getGameInteractions();
		updater = ((StalkerApplication) this.getApplication())
				.getUpdater();
		
		
		

		mapView = (MapView) this.findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);
		mapController = this.mapView.getController();
		mapController.setZoom(StalkerApplication.INITIAL_ZOOM_LEVEL);
		
		
		// Get touch coordinates
/*
	    mapView.setOnTouchListener(new View.OnTouchListener() {
	        @Override
	        public boolean onTouch(View v, MotionEvent event) {
	            touchX = (int) event.getX();
	            touchY = (int) event.getY();
	                return true;
	        }
	    });*/
		
		//Creating the bitmaps
		if (currentPlayer.getUserID()==49){ 
		youBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.hat_fez);} 
		else youBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.you);
		
		youBmDrawable = new BitmapDrawable(getResources(),youBitmap);
		
		victimBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.victim);
		victimBitapBitmapDrawable= new BitmapDrawable(getResources(),victimBitmap);
		 
		/* Notify updater it should wake and update as if a gui is watching.*/
		if (updater!=null){
			((StalkerApplication) this.getApplication()).setInGui(true);
			Log.d("Application", "Updater Not null. Notifying.");
			synchronized (updater) {
			updater.notify();	
		}}
		
		/* create rp object that is required by the Overlays*/
		resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());
		instructionsView = (TextView) findViewById(R.id.start_game_instructions);
		
		/* Update instructions*/
		if (preferences.getBoolean("PREF_SHOW_INSTRUCTIONS", true)) {
			
				new CountDownTimer(9000, 500) {
			     public void onTick(long millisUntilFinished) {
			    	if (millisUntilFinished>6000){
			 		instructionsView.setText(R.string.instruction_how_to_start_general);}
			    	else if (millisUntilFinished>3000){
					instructionsView.setText(R.string.instruction_how_to_start_tap);}
			    	else{
					instructionsView.setText(R.string.instruction_how_to_start_press);
			    	}
			     }

			     public void onFinish() {
			         this.start();
			     }
			  }.start();
			

		}
			
			/* Load new players and center on them*/
		new GetVictimsAsync(true).execute(currentPlayer);
			
			/*Post update in 3 seconds and repeating...*/
			handler.postDelayed(runnable, 3000);
	}

	public void onResume(Bundle savedInstanceState) {
		super.onResume();

		/* Update Application level preferences to be the settings of this acticvity.*/
				preferences = PreferenceManager
						.getDefaultSharedPreferences(getApplicationContext());

				/* Use the shared Application objects*/
				currentPlayer = ((StalkerApplication) this.getApplication())
						.getCurrentPlayer();
				gameInteractions = ((StalkerApplication) this.getApplication())
						.getGameInteractions();
				
				getVictimsAsync.execute(currentPlayer);		
		}
	
	
 public void onPause(){
	 super.onPause();
	 
	 ((StalkerApplication) this.getApplication()).setInGui(false);
	 Log.d(TAG, "Pausing start game activity");
	 /*Stopping repeated updating of GUI*/
	 handler.removeCallbacksAndMessages(null);

 }
 
	protected void onResume(){
		super.onResume();
		/*Resume repeated updating of GUI*/
		Log.d(TAG, "Resuming interaction activity");
		handler.post(runnable);
	}
 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class GetVictimsAsync extends AsyncTask<Player, Integer, ArrayList<Player>>
	{
		private boolean centerOnPlayer;
		
		public GetVictimsAsync(boolean centerOnPlayer) {
			super();
			this.centerOnPlayer = centerOnPlayer;
		}

		@Override
		protected ArrayList<Player> doInBackground(Player... players) {
			ArrayList<Player> ret = null;
			try {
				Log.d(TAG, "Getting adjacent players");
				ret = ((StalkerApplication) outer.getApplication()).getAdjacentPlayers();
				
			} catch (Exception e) {
				e.printStackTrace();
				Log.getStackTraceString(e);
				Log.d(TAG, e.getMessage() + " ID: " + currentPlayer.getUserID());
				cancel(true);
			}

			return ret;
		}
		
		@Override
		protected void onPostExecute(ArrayList<Player> players){
			
			/* GP object to use for player		*/
			GeoPoint gp;
			
			/* Center point of map, current players location.*/
			GeoPoint centerGp = new GeoPoint(currentPlayer.getLat(),currentPlayer.getLon());
			Log.d(TAG, "You @ " + currentPlayer.getLat()+", "+currentPlayer.getLon());
			
			/*Clears and refills items lists. (Should only have one item at the time...)*/
			playerItems.clear();
			victimItems.clear();
			
			/*Add all players within a specific range*/
			if (players != null){
			for (int i = 0; i < players.size(); i++) {
				Log.d(TAG, "Foud players. Printing");
				Player activePlayer = players.get(i);
				double distance = GameInteraction.haversin(currentPlayer.getLat(), currentPlayer.getLon(), activePlayer.getLat(), activePlayer.getLon());
				gp = new GeoPoint(activePlayer.getLat(),activePlayer.getLon());
				
				VictimsOverlayItem playerItem = new VictimsOverlayItem("" + activePlayer.getUserID(), "A player to stalk @distance: " + distance, gp, activePlayer.getUserID());
				// Change its apperence 

				playerItem.setMarker(victimBitapBitmapDrawable);
				victimItems.add(playerItem);
							}
			}
			
			
			/*Add current player as item.*/
			OverlayItem you = new OverlayItem("This is you"," You", new GeoPoint(currentPlayer.getLat(), currentPlayer.getLon()));
			
			/*
			 * Change look of it.
			 */
			you.setMarker(youBmDrawable);

			playerItems.add(you);
			
			/* Enter if it should be centered around player.*/
			if (centerOnPlayer){
			mapController.setCenter(new GeoPoint(currentPlayer.getLat(), currentPlayer.getLon()));
			}
			
			Log.d(TAG, "Center set @ " + centerGp.getLatitudeE6() +", " + centerGp.getLongitudeE6());
			
			/* Create the overlay and the touch event listeners.*/
			victimsOverlay = new VictimsOverlay(victimItems, new OnItemGestureListener<VictimsOverlayItem>(){
				
				public boolean onItemSingleTapUp(final int arg0, final VictimsOverlayItem item) {
					
						currentItem = item;


										try {
						// We need to get the instance of the LayoutInflater
						LayoutInflater inflater = (LayoutInflater) StartGameActivity.this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						View layout = inflater.inflate(R.layout.victim_popup_window,(ViewGroup)
						
								findViewById(R.id.popup_group));
						layout.setAlpha(1);
						
						victimInfo = (TextView) layout.findViewById(R.id.victim_info);
						victimInfo.setText("Player:"+item.mTitle + " and " + item.mDescription);
						
						View parent = (View) findViewById(R.id.start_game_image);
						pwindo = new PopupWindow(layout, 350, 350, true);
						pwindo.showAtLocation(parent, Gravity.BOTTOM, 10, 10);
						
						
						closeButton = (Button) layout.findViewById(R.id.close_victim_info);
						stalkButton = (Button) layout.findViewById(R.id.stalk_victim);
						friendButton = (Button)layout.findViewById(R.id.friend_victim);
						
						
						closeButton.setOnClickListener(cancelButtonClickListener);
						stalkButton.setOnClickListener(stalkButtonClickListener);

						} catch (Exception e) {
						e.printStackTrace();
						}
					
					

					
					return true;
				}
				
				OnClickListener stalkButtonClickListener = new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(getApplicationContext(),GameInteractionActivity.class);
					     intent.putExtra(getString(R.string.id_to_stalk), currentItem.getUserID());
					     startActivity(intent);
					     pwindo.dismiss();
					     /* Must end this activity here or else the Activity stack gets really funny.*/
					     finish();
					}
				};
				//class ItemizedOnClickListener extends View {};
				
				OnClickListener cancelButtonClickListener = new OnClickListener(){
					@Override
					public void onClick(View v) {
					Log.d(TAG, "Fired");	
					pwindo.dismiss();
					}
					
				};
			@Override
				public boolean onItemLongPress(final int arg0, final VictimsOverlayItem item) {
				     Intent intent = new Intent(getApplicationContext(),GameInteractionActivity.class);
				     Log.d(TAG, "Passing intent w: "+ item.getUserID());
				     intent.putExtra(getString(R.string.id_to_stalk), item.getUserID());
				     startActivity(intent);
				     
				     /* Must end this activity here or else the Activity stack gets really funny.*/
				     finish();
				     
					return false;
				}} , resourceProxy, centerGp);
			
			playerOverlay = new ItemizedOverlayWithFocus<OverlayItem> (playerItems, null, resourceProxy);
			
			mapView.getOverlays().clear();
			mapView.getOverlays().add(victimsOverlay);
			mapView.getOverlays().add(playerOverlay);
			mapView.invalidate();
		}	
	}
	
	private class VictimsOverlay extends ItemizedOverlayWithFocus<VictimsOverlayItem>{
		
		GeoPoint playerGp;
		public VictimsOverlay(
				List<VictimsOverlayItem> aList,
				org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener<VictimsOverlayItem> aOnItemTapListener,
				ResourceProxy pResourceProxy, GeoPoint playerGp) {
			super(aList, aOnItemTapListener, pResourceProxy);
			
			this.playerGp = playerGp;
			
		}
		
		
		@Override
		/***
		 * This method draws circles on the map where the radius is touching (Rather almost touching, geodesi is difficult to get precise) 
		 * the position of each stalker.
		 */
		
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, shadow);
						
			
			Point startScreenPts = new Point();
			
			/* Convert center geopoint to pixels on the map.*/
			mapView.getProjection().toPixels(playerGp, startScreenPts);
			
			float outerRadius; // Radius in pixels
			float innerRadius; // Radius in pixels
			
			/**
			 * Just stuff to set up a proper paint class for the circle.
			 */

			Paint paintOuter = new Paint(Paint.ANTI_ALIAS_FLAG);
			paintOuter.setColor(Color.RED);
			paintOuter.setStyle(Paint.Style.STROKE);
			paintOuter.setStrokeCap(Paint.Cap.ROUND);
			paintOuter.setStrokeWidth(6);

			Paint paintInner = new Paint(Paint.ANTI_ALIAS_FLAG);
			paintInner.setColor(Color.RED);
			paintInner.setStrokeCap(Paint.Cap.ROUND);
			//Make circle semitransparent
			paintInner.setAlpha(50);
				
				
				
				/*
				 * This is the lat of the stalked (center point) for geodesi relations
				 */
				Double lat = currentPlayer.getLat();
				
				/* 
				 * Ugly typcasting but it is beacuse of getDistance normaly
				 * returns an object of type Double and
				 * Now it must be float.
				 * 
				 * This float is the distance i km between the two players in the interaction.
				 */
				
				/*Calculate the actual radius on the plotted map from the distance in km * 1000 to make it to meters.*/
				/* convert distance in meters to pixels.*/
				innerRadius = GameInteractionActivity.metersToRadius((float) (StalkerApplication.OUT_OF_RANGE * 1000), mapView, lat); 
				/* convert distance in meters to pixels.*/
				outerRadius = GameInteractionActivity.metersToRadius((float) (StalkerApplication.OUT_OF_RANGE * 2 * 1000), mapView, lat); 
				
				/* Do the actual drawing of the circle using what was prepared.*/
				canvas.drawCircle(startScreenPts.x, startScreenPts.y, outerRadius, paintOuter);
				canvas.drawCircle(startScreenPts.x, startScreenPts.y, innerRadius, paintInner);
				
		}
	}
}

