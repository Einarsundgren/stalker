/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

/**
 * The starting point of the app. This is what android loads as the initial activity.
 * Has two functions. To display a starting screen and to load and check the essential background parts of the game.
 * @author Einar Sundgren
 * @version 1.0
 *
 */

public class LoadActivity extends Activity {
	
	/* Shared player object*/
	private Player currentPlayer;
	/* Shared GameServer object*/
	private GameServer gameServer;

	/* Create prefs */
	SharedPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load);
		preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		/* The main loading is done in an async task.*/
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/* Inflate the menu; this adds items to the action bar if it is present.*/
		getMenuInflater().inflate(R.menu.load, menu);
		return true;
	}
	
	@Override
	public void onResume(){
		super.onResume();
		//Load again if paused. You never know what happened.
		new LoadApplication().execute(this);
	}
	
	@Override
	public void onPause(){
		super.onPause();
		// When this activity is paused it should be removed.
		//finish();
		
	}

	/**
	 * The async task that loads all the game functions in the background and updates the gui.
	 * @author Einar Sundgren
	 * @version 1.0
	 */
	private class LoadApplication extends AsyncTask<Activity, UpdateStatus, Activity>{
		
		private static final String TAG = "LoaderActivity";
		protected TextView textView;
		protected boolean playerFailedToLoad;
		protected boolean networkFailedToLoad;
		protected boolean positioningFailedToLoad;
		protected boolean serverFailedToLoad;

		@Override
		protected MainActivity doInBackground(Activity... activities) {			
			
			String serverName = getApplicationContext().getResources().getString(R.string.default_server_URI);
					//preferences.getString("PREF_GAME_SERVER_URI", "Default");
			String userName = preferences.getString("PREF_GAME_SERVER_USER", "0");
			String password = preferences.getString("PREF_GAME_SERVER_PASSWORD", "0");
			
			
			/* Starting location service...*/
			LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
			boolean enabled = service
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			/* Check if enabled and if not send user to the GSP settings
			 * Better solution would be to display a dialog and suggesting to
			 * go to the settings.
			 */
			//TODO: Activity that shows no GPS. And lets you continue.
			if (!enabled) {
				Log.d(TAG,"Positioning not enabled");
				positioningFailedToLoad=true;
				cancel(true);
				publishProgress(new UpdateStatus(UpdateStatus.LOADED_POSITION,UpdateStatus.FAIL));
				
			} else {Log.d(TAG,"Positioning enabled");
			publishProgress(new UpdateStatus(UpdateStatus.LOADED_POSITION,UpdateStatus.SUCCESS));
			};
					
			/*Test if network is available*/
			if (StalkerApplication.isNetworkAvailable(activities[0]))
				{
					publishProgress(new UpdateStatus(UpdateStatus.LOADED_NETWORK, UpdateStatus.SUCCESS));
					} 
			else {
				networkFailedToLoad=true;
				cancel(true);
				}
			
			/* Load server*/
			Log.d(TAG,"Gameserver=" + serverName);
			gameServer = new GameServer(serverName);
			gameServer.setDist(StalkerApplication.OUT_OF_RANGE);
			gameServer.setNumPlayers(10);
			((StalkerApplication) activities[0].getApplication()).setGameServer(gameServer);
			
			if (gameServer != null){
			publishProgress(new UpdateStatus(UpdateStatus.LOADED_SERVER, UpdateStatus.SUCCESS));
			
			} else {
				serverFailedToLoad = true;
				cancel(true);}			
			/*
			 * Load player from server using credentials.
			 */
			Log.d(TAG, "Trying to load player using: " + userName +" and pw " + password);
			currentPlayer = gameServer.getPlayerInfo(new Player(userName, password));
			
			if (currentPlayer != null){
				Log.d(TAG, "Player object populated with " + currentPlayer.getUserID());
				Log.d(TAG, "Player loaded.");
				((StalkerApplication) activities[0].getApplication())
				.setCurrentPlayer(currentPlayer);
				
			publishProgress(new UpdateStatus(UpdateStatus.LOADED_PLAYER, UpdateStatus.SUCCESS));
			
			} else {
				playerFailedToLoad=true;
				cancel(true);
			    }
			
			
			return null;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			
			if (positioningFailedToLoad){
				Log.d(TAG,"Pos failed to load");
				Intent intent = new Intent(getApplicationContext(),GPSOnActivity.class);
				startActivity(intent);
				//finish();
			}
			else if (networkFailedToLoad){
				Log.d(TAG,"Network failed to load");
				Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
				startActivity(intent);
				//finish();
			}
			else if (serverFailedToLoad){
				Log.d(TAG,"Server failed to load");
			}
			
			else if (playerFailedToLoad){
			Log.d(TAG, "Player object null");
			Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
			startActivity(intent);
			//finish();
			}
		}

		@Override
		protected void onPostExecute(Activity result) {
			super.onPostExecute(result);
			/* If we got here we can assume no GPS fail. Start location updater service*/
			Intent setLocationService = new Intent(getApplicationContext(), SetLocation.class);
			getApplicationContext().startService(setLocationService);	
			
			/*
			 * And load the main menu intent.
			 */
			Intent intent = new Intent(getApplicationContext(), MainActivity.class);
			startActivity(intent);
		}

		@Override
		protected void onProgressUpdate(UpdateStatus... values) {
			super.onProgressUpdate(values);
			String status = "";
			
			if (values[0].getUpdatedItem() == UpdateStatus.LOADED_PLAYER){
				if(values[0].getStatus()==UpdateStatus.SUCCESS)
				{
					Log.d(TAG,"Could load player");
					status = "success";}
				
				else{
					Log.d(TAG,"cancelled bco no player");
					status = "fail";
				}
				textView = (TextView) findViewById(R.id.load_message);
				textView.append("\n" + getString(R.string.loading_player) + " " + status);
			}
			
			/* If network loads*/
			if (values[0].getUpdatedItem() == UpdateStatus.LOADED_NETWORK){
				if(values[0].getStatus()==UpdateStatus.SUCCESS)
				{
					Log.d(TAG,"Could load network");
					status = "success";}
				else{
					Log.d(TAG,"cancelled bco no network");
					status = "fail";
				}
				textView = (TextView) findViewById(R.id.load_message);
				textView.append("\n" + getString(R.string.loading_network) + " " + status);
			}
			
			
			if (values[0].getUpdatedItem() == UpdateStatus.LOADED_SERVER){
				if(values[0].getStatus()==UpdateStatus.SUCCESS)
				{status = "success";
				Log.d(TAG,"Could load server");
				}
				else{
					status = "fail";
				}
				textView = (TextView) findViewById(R.id.load_message);
				textView.append("\n" + getString(R.string.loading_server) + " " + status);
			}
			
			if (values[0].getUpdatedItem() == UpdateStatus.LOADED_POSITION){
				if(values[0].getStatus()==UpdateStatus.SUCCESS)
				{status = "success";
				Log.d(TAG,"Could load position");}
				else{
					Log.d(TAG,"cancelled bco no pos");

					
					status = "fail";
				}
				textView = (TextView) findViewById(R.id.load_message);
				textView.append("\n" + getString(R.string.loading_position) + " " + status);
			}
			
		}
		
	}
	
}
