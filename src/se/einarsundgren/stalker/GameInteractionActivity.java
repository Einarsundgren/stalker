/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;

import java.util.ArrayList;
/***
 * The activity displaying the interactions between the current player and other players. A stalked player is displayed as a map marker. 
 * A stalking player is displayed as a red area so the current player dont know its exact location. Only the aproximate distance.
 * @author Einar Sundgren
 * @version 1.0
 */
public class GameInteractionActivity extends Activity {

	private final String TAG = "InteractionActivity";
	/* Shared player object*/
	private Player currentPlayer;
	/* Shared GameServer object*/
	private GameServer gameServer;
	/* Create prefs*/
	SharedPreferences preferences;
	
	/**
	 *  Stuff for Osm droid.
	 */
	/* Overlay that prints the current players location*/
	protected ItemizedOverlayWithFocus<VictimsOverlayItem> currentPlayerOverlay;

	/* Overlay that prints the overlay with players stalked by current player*/
	protected ItemizedOverlayWithFocus<VictimsOverlayItem> stalkedByCurrentPlayerOverlay;

	/* This is an extended ItemizedOverlayWithfocus that prints users that are
	 stalking the current player.*/
	protected StalkingCurrentPlayerOverlay stalkingCurrentPlayerOverlay;
	
	/* List of OverlayItmes showing stalked users*/
	protected ArrayList<VictimsOverlayItem> stalkedForMap = new ArrayList<VictimsOverlayItem>();
	
	/* List of OverlayItmes showing current users*/
	protected ArrayList<VictimsOverlayItem> markCurrent = new ArrayList<VictimsOverlayItem>();
	
	/* List of OverlayItmes showing stalking users*/
	protected  ArrayList<InteractionOverlayItem> stalkingForMap = new ArrayList<InteractionOverlayItem>();

	/* Supporting objects for OSM droid. */
	private ResourceProxy resourceProxy;
	private MapView mapView;
	private MapController mapController;
	protected Context activityContext;
	protected Activity currentActivity;
	private GeoPoint centerGp;
	
	/*Textview that displays information to the user. If info is turned on.*/
	protected TextView instructionsView;
	
	/* Async object that is used to begin stalking a new player.*/
	protected StalkNewAsync stalkNewAsync  = new StalkNewAsync();
	
	/* The async class that retrieves player objects and prints the maps.*/  
	protected GetInteractionsAsync getInteractionsAsync = new GetInteractionsAsync(true);
	
	/* This class to be passed to some of its supporting classes.*/
	protected GameInteractionActivity outer = this;
	
	/* Handler and runnable to update the gui thread on specific intervals*/
	private Handler handler = new Handler();
	private Runnable runnable = new Runnable() {
		   @Override
		   public void run() {
		      /* Things to do on interval
				Refresh GUI but don�t center on player.*/
				   new GetInteractionsAsync(StalkerApplication.DONT_CENTER_ON_PLAYER).execute(currentPlayer);
			   /* Call itself again with the same interval.
			   A short sleep of this thread since the gui should be responsive.*/
		      handler.postDelayed(this, StalkerApplication.MICRO_SLEEP);
		   }
		};
		
	/* A progressbar showing the time of the current stalking.	*/
	protected ProgressBar time;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_game_interaction);

		/***
		 * Setting up some of the shared objects.
		 */
		/* Set Application level preferences to be the settings of this
		 acticvity.*/
		preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		/* Use the shared Application objects*/
		gameServer = ((StalkerApplication) this.getApplication())
				.getGameServer();
		currentPlayer = ((StalkerApplication) this.getApplication())
				.getCurrentPlayer();
		
		/*Using shared updater objects. This is the game logic class.*/
		Updater updater = ((StalkerApplication) this.getApplication())
				.getUpdater();
		
		/* Get the view to update with instructions. */
		instructionsView = (TextView) findViewById(R.id.interactions_instruction);


		Intent startIntent = getIntent(); // gets the previously created intent
		/* will return string identifying the parameter */
		int idToStalk = startIntent.getIntExtra(getString(R.string.id_to_stalk),0); 
		
		/* If the intent contains the id_to_stalk, call the server with the id of the user to stalk 
		 before plotting the map. (They actually run in parallel so the GUI might miss it. Never happened though. 
		 And if it does it updates each 2 seconds so no worries.*/

		if (idToStalk > 0){
			Log.d(TAG, "Start stalking user w id: " + idToStalk);
			
			/*Using an array of Players to be able to pass more than one to player objects 
			 as parameters to the async task.*/

			Player[] tmpPlayers = new Player[2];
			// Current player is player 0 and the stalked one is player 1
			tmpPlayers[0]=currentPlayer;
			tmpPlayers[1]=new Player(idToStalk);
			
			/*Stalk recently added players and print them*/
			stalkNewAsync.execute(tmpPlayers);
			
		}else {
			Log.d(TAG, "Failed to stalking user w id: " + idToStalk);
		}

		 
		/* Create the basis of the map.*/
		mapView = (MapView) this.findViewById(R.id.gameInteractionMap);
		mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);
		mapController = this.mapView.getController();
		
		//TODO: Set zoomlevel to be appropriate 
		mapController.setZoom(StalkerApplication.INITIAL_ZOOM_LEVEL);
		
		/* Checking for null object is just a precaution. Happened during debug but 
		 not likely to happen in a stable version.*/
		if (updater!=null){
			((StalkerApplication) this.getApplication()).setInGui(true);
			Log.d("Application", "Updater Not null. Notifying.");
			synchronized (updater) {
			updater.notify();	
		}}
		
		/* Actually prints all objects and overlays and map stuff.*/
		getInteractionsAsync.execute(currentPlayer);
		
		/* Creates the progressbar showing the age of the stalk.*/
		time = (ProgressBar) findViewById(R.id.interaction_lifetime_timer);
		time.setMax(StalkerApplication.STALKING_TIME_LIMIT);
		
		/* Starts the handler that updates the GUI with the micro sleep interval.*/
		handler.postDelayed(runnable, 6000);
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_interaction, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			// If settings selected, open settings activity.
			Intent intent = new Intent(this, StalkerPreferenceActivity.class);
			startActivityForResult(intent, 1);
			return true;

		default:
			return false;
		}
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		 Log.d(TAG, "Pausing interaction activity");

		 // Tell updater that the gui is not running.
		 ((StalkerApplication) this.getApplication()).setInGui(false);
		//Stopping repeated updating of GUI
		 handler.removeCallbacksAndMessages(null);
	}
	
	protected void onResume(){
		super.onResume();
		Log.d(TAG, "Resuming interaction activity");
		//Resuming repeated updating of GUI
		handler.postDelayed(runnable, 100);
	}
	
	
/**
 * Everything below this line is a candidate for moving to external files.	
 */
	//TODO: Externalize as many of the interal classes as possible.
	//TODO: Make user registration screen.
	//TODO: Make sure no crash if disconnected/No valid player or other network error.
	//TODO: Better queries that don't include already stalked players
		
	/***
	 * Async class to start stalking new player
	 * @author Einar
	 *
	 */
	private class StalkNewAsync extends AsyncTask<Player[], Integer, Integer>{

	protected Integer doInBackground(Player[]... players) {
		gameServer.stalkPlayer(players[0][0], players[0][1]);
		return null;
	}
		
	}
	
	/***
	 * Async class to stop stalking a player
	 * @author Einar
	 *
	 */
	private class StopStalkingAsync extends AsyncTask<Player[], Integer, Integer>{
		@Override
		protected Integer doInBackground(Player[]... players) {
			gameServer.stopStalking(players[0][0], players[0][1]);
			return null;
		}
		
	}
	
	/***
	 * Private Async class that handles getting the players and positioning of them on the map.
	 * Not needed to be async since the network contact is done by the Service thread.
	 * Still a convenient way of organizing the code.
	 * @author Einar
	 *
	 */
	public class GetInteractionsAsync extends AsyncTask<Player, Integer, ArrayList<GameInteraction>> {

		protected boolean centerOnPlayer;
		
		/* Overided constructor with boolean that tells if you should center map on player or not.
		 A user should be able to move around the map without it reseting every time.*/
		public GetInteractionsAsync(boolean centerOnPlayer) {
			super();
			this.centerOnPlayer = centerOnPlayer;
		}

		/**
		 * Gets the interactions supplied by the shared updater.
		 */
		@Override
		protected ArrayList<GameInteraction> doInBackground(Player... players) {
			ArrayList<GameInteraction> ret = null;
			try {
				/* Get list of game interactions from shared object.*/
				//TODO: Is it enough to use the earlier referenced object? Probably. 
				ret = ((StalkerApplication) outer.getApplication()).getGameInteractions(); 
			
			} catch (Exception e) {
				e.printStackTrace();
				Log.d(TAG, " gameInteractions Error: " + e.getMessage());
				cancel(true);
			}
			return ret;
		}

		
		/*
		 * Basically handles to plot the retreived game interactions on the map
		 */
		@Override
		protected void onPostExecute(ArrayList<GameInteraction> s) {
			super.onPostExecute(s);
			
			/*Clear old lists of map items*/
			stalkingForMap.clear();
			stalkedForMap.clear();
			markCurrent.clear();
			
			/* Is set when the current user is displayed on map*/
			boolean playerSet = false;

			/* Temporay GeoPoint object used to carry lat/lon to OSM*/
			GeoPoint gp;
			
			/* Center point of map, current players location.*/
			centerGp = new GeoPoint(currentPlayer.getLat(),currentPlayer.getLon());
			
			/* Enter if any interactions was returned.*/
			if (s!= null){
				
			/* Iterate over all Interaction items retreived.*/
			for (int i = 0; i < s.size(); i++) {
				GameInteraction currentInteraction = s.get(i);
				Player stalker = currentInteraction.getStalker();
				Player stalked = currentInteraction.getStalked();
				
				/**
				 * Need two varations of this since its not certain which will
				 * come first from the query. 
				 */
				
				Log.d(TAG, "Stalker: " + stalker.getUserName() + " stalked: " + stalked.getUserName());
				
				/* Check if current player is stalked in this interaction*/
				if (stalked.getUserID() == currentPlayer
						.getUserID()) {

					/* If not set, set currently stalked player as center of map.*/
					if (!playerSet) {
						centerGp = new GeoPoint(stalked.getLat(), stalked.getLon());

						playerSet = true;
						Log.d(TAG, "User position set @ " + centerGp.getLatitudeE6() +", " + centerGp.getLongitudeE6());
					}
					gp = new GeoPoint(stalker.getLat(),stalker.getLon());
					
					if (preferences.getBoolean("PREF_SHOW_INSTRUCTIONS", true)) {
						instructionsView.setText(R.string.interactions_escape);
					}
					
					stalkingForMap.add(new InteractionOverlayItem(gp,
							currentInteraction, "A stalker of you at "+ "lat: " + stalker.getLat() +" lon: " + stalker.getLon()));

				}
				/* Not stalked, should be stalker then. Otherwise smthng is
				 really wrong and an exception should be thrown. */
				else if (stalker.getUserID() == currentPlayer.getUserID()) {

					/* If this is the first iteration, the center is not yet set. So do that.*/
					if (!playerSet) {
						centerGp = new GeoPoint(stalker.getLat(), stalker.getLon());
						playerSet = true;
						Log.d(TAG, "User position set by 2 @ " + centerGp.getLatitudeE6() +", " + centerGp.getLongitudeE6());
					}
					/* So we know player is the stalker in this interaction.
					 *  Print the stalked players location.
					 */
					gp = new GeoPoint(stalked.getLat(),stalked.getLon());
					
					/*update progressbar with age. */
					time.setProgress((int)currentInteraction.getAge());
					
					/* Update instructions depending on distance, if they are switched on*/
					if (preferences.getBoolean("PREF_SHOW_INSTRUCTIONS", true)) {
						if  (currentInteraction.getDistance()<StalkerApplication.OUT_OF_RANGE && 
								currentInteraction.getDistance()>=StalkerApplication.FAR
								){ instructionsView.setText(R.string.interactions_stalking_far);}
						
						else if (currentInteraction.getDistance()<StalkerApplication.FAR&&
								currentInteraction.getDistance()>=StalkerApplication.NEAR
								){ instructionsView.setText(R.string.interactions_stalking_near);
						}
						
						else if (currentInteraction.getDistance()<StalkerApplication.NEAR&&
								currentInteraction.getDistance()>=StalkerApplication.ON_TOP){ 
							instructionsView.setText(R.string.interactions_stalking_on_top);
						}
						else if (currentInteraction.getDistance()<StalkerApplication.ON_TOP) {
							instructionsView.setText(R.string.interactions_stalking_bulls_eye);
						} else {
							instructionsView.setText(R.string.interactions_get_in_range);
						}
					}
					
					/* Add current stalker to the arraylist of OverlayItems map with a message.*/
					
					VictimsOverlayItem stalkedItem = new VictimsOverlayItem(stalked.getUserName(),
							"The user you are stalking at lat: " + stalked.getLat() +" lon: " + stalked.getLon(), gp,stalked.getUserID());
					Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.victim);
					stalkedItem.setMarker(new BitmapDrawable(getResources(),bitmap));
					stalkedForMap.add(stalkedItem);
				}
			}
		}else {
			/* No interactions is alive. Just set player coordinates as center.*/
			centerGp = new GeoPoint(currentPlayer.getLat(), currentPlayer.getLon());
			
			/* Update instructions*/
			if (preferences.getBoolean("PREF_SHOW_INSTRUCTIONS", true)) {
			instructionsView.setText(R.string.interactions_none);
			}
		}


			/* Set what should be the center of the if flag is set. */
			if (this.centerOnPlayer){
			mapController.setCenter(centerGp);
			}
			

			/* create the proxy object that is required by the Overlays*/
			resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());
			
			/*
			 * Fix the placing of the user.
			 */
			VictimsOverlayItem you = new VictimsOverlayItem ("This is you ",currentPlayer.getUserName(), centerGp );
			Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.you);
			you.setMarker(new BitmapDrawable(getResources(),bitmap));
			markCurrent.add(you);
			
			/* 
			 * Create three overlays of the three different arraylists of different player roles.
			 */
			currentPlayerOverlay = new StalkedByCurrentPlayer(markCurrent, itemGestureListener, resourceProxy);
					
			stalkingCurrentPlayerOverlay = new StalkingCurrentPlayerOverlay(
					stalkingForMap, itemGestureListener, resourceProxy);

			stalkedByCurrentPlayerOverlay = new ItemizedOverlayWithFocus<VictimsOverlayItem>(
					stalkedForMap, itemGestureListener, resourceProxy);

			/* Add the finished overlays to the map view and update it*/
			mapView.getOverlays().clear();
			mapView.getOverlays().add(stalkingCurrentPlayerOverlay);
			mapView.getOverlays().add(stalkedByCurrentPlayerOverlay);
			mapView.getOverlays().add(currentPlayerOverlay);
			mapView.invalidate();
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			//You should not get here. That would be a strange error.
			Log.d(TAG, "Printing game interactions cancelled.");

		}
	}

	/***
	 * 
	 * Class that defines the overlay showing stalkers of the current player. 
	 * Actual users are not printed unless in debug mode. Instead an area is painted red.
	 * @author Einar Sundgren
	 *
	 */
	
	private class StalkingCurrentPlayerOverlay extends ItemizedOverlayWithFocus<InteractionOverlayItem> {
		private GeoPoint startGeoPoint;
		ArrayList<InteractionOverlayItem> interactionOverlayItems;

		/* 
		 * I know. Its ugly to supress warnings. Create a special overlay class for current user to get to terms with this.
		 */
		@SuppressWarnings({ "rawtypes", "unchecked" })
		
		public StalkingCurrentPlayerOverlay(
				ArrayList<InteractionOverlayItem> aList,
				OnItemGestureListener aOnItemTapListener,
				ResourceProxy pResourceProxy) {
			
			super(aList, aOnItemTapListener, pResourceProxy);			
			this.startGeoPoint = new GeoPoint(currentPlayer.getLat(),currentPlayer.getLon());
			this.interactionOverlayItems = aList;
		}
		

		
		@Override
		/***
		 * This method draws circles on the map where the radius is touching 
		 * (Almost touching, geodesi calculations are difficult) 
		 * the position of each stalker.
		 */
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			/**
			 * Show the exact position of the stalkers for debugging if that is selected in settings.
			 */
			if (preferences.getBoolean("DEBUG_SHOW_STALKERS_EXACT", false)) super.draw(canvas, mapView, shadow);
			
			Point startScreenPts = new Point();
			
			/*
			 *  Convert center geopoint to pixels on the map.
			 */
			mapView.getProjection().toPixels(this.startGeoPoint, startScreenPts);
			float radius; // Radius in pixels
			
			/**
			 * Just stuff to set up a proper paint class for the circle.
			 */

			Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
			paint.setColor(Color.RED);
			paint.setStrokeCap(Paint.Cap.ROUND);
			//Make circle semitransparent
			paint.setAlpha(50);
			for (int i = 0; i < interactionOverlayItems.size(); i++) {
				
				/* 
				 * This float is the distance i km between the two players in the interaction.
				 */
				Float distance = interactionOverlayItems.get(i).gameInteraction.getDistance().floatValue();
				
				/*
				 * This is the lat of the stalked (center point) for geodesi relations
				 */
				Double lat = interactionOverlayItems.get(i).gameInteraction.getStalked().getLat();
				
				/*
				 * Calculate the actual radius on the plotted map from the distance in km * 1000 to make it to meters.
				 */
				radius = metersToRadius(distance * 1000, mapView, lat); // convert distance in meters to pixels.

				Log.d("TAG", "Radius pixels: " + Float.toString(radius) + " iteration " + i);
				
				/*
				 *  Do the actual drawing of the circle using what was prepared.
				 */
				canvas.drawCircle(startScreenPts.x, startScreenPts.y, radius, paint);
			}
			
			
			
		}
		
	}
	
	
	/***
	 * Class that defines the behavior of the overlay Showing 
	 * the players that are beeing stalked by the current player
	 * @author Einar Sundgren
	 *
	 */
	
	private class StalkedByCurrentPlayer extends ItemizedOverlayWithFocus<VictimsOverlayItem> {
		public StalkedByCurrentPlayer(ArrayList<VictimsOverlayItem> aList,
				OnItemGestureListener<VictimsOverlayItem> aOnItemTapListener,
				ResourceProxy pResourceProxy) {
			super(aList, aOnItemTapListener, pResourceProxy);
		}
		
		/*
		 *  This draws green cirles showing the points for each stalked player.(non-Javadoc)
		 * @see org.osmdroid.views.overlay.ItemizedOverlayWithFocus#draw(android.graphics.Canvas, org.osmdroid.views.MapView, boolean)
		 */
		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow){
			super.draw(canvas, mapView, shadow);
			Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
			paint.setColor(Color.GREEN);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeCap(Paint.Cap.ROUND);
			
			Double lat = currentPlayer.getLat();
			int radius;
			Point startScreenPts = new Point();
			
			/*
			 *  Convert center geopoint to pixelon the map.
			 */
			mapView.getProjection().toPixels(new GeoPoint(currentPlayer.getLat(), currentPlayer.getLon()), startScreenPts);
			
			/*
			 * Draw circles for different ranges.
			 */
			paint.setStrokeWidth(6);
			radius = GameInteractionActivity.metersToRadius((float) (StalkerApplication.OUT_OF_RANGE * 1000), mapView, lat); // convert distance in meters to pixels.
			canvas.drawCircle(startScreenPts.x, startScreenPts.y, radius, paint);

			paint.setStrokeWidth(4);
			radius = GameInteractionActivity.metersToRadius((float) (StalkerApplication.FAR * 1000), mapView, lat); // convert distance in meters to pixels.
			canvas.drawCircle(startScreenPts.x, startScreenPts.y, radius, paint);
			
			paint.setStrokeWidth(2);
			radius = GameInteractionActivity.metersToRadius((float) (StalkerApplication.NEAR * 1000), mapView, lat); // convert distance in meters to pixels.
			canvas.drawCircle(startScreenPts.x, startScreenPts.y, radius, paint);
			
			paint.setStrokeWidth(1);
			radius = GameInteractionActivity.metersToRadius((float) (StalkerApplication.ON_TOP * 1000), mapView, lat); // convert distance in meters to pixels.
			canvas.drawCircle(startScreenPts.x, startScreenPts.y, radius, paint);
		}
		
		
	}
	
	
	/***
	 * 
	 * Class that defines the behaviour of the overlay showing the current player. 
	 * Mainly for consistency wit the two other layers. Not used in version 1.0 but should be 
	 * put to use for better maintainability and readability.
	 * @author Einar Sundgren
	 * @version 1.0
	 *
	 */
	private class CurrentPlayerOverlay extends ItemizedOverlayWithFocus {
		public CurrentPlayerOverlay(ArrayList<InteractionOverlayItem> aList,
				OnItemGestureListener aOnItemTapListener,
				ResourceProxy pResourceProxy) {
			super(aList, aOnItemTapListener, pResourceProxy);
		}
		
	}
	
	
/***
 * Generalizing an event listener for touch events on all the map needles on any
 * stalked player. 
 * 
 */
	 // TODO Might need to scrap this generalization later for some specializations
	 // TODO: Refactor the name of this class to Stalking something for consistency.
	
	private final OnItemGestureListener<VictimsOverlayItem> itemGestureListener = new ItemizedIconOverlay.OnItemGestureListener<VictimsOverlayItem>() {

		@Override
		public boolean onItemSingleTapUp(final int index, final VictimsOverlayItem item) {
			Toast.makeText(
					//TODO: Remove hardcoded debug text and replace with R.string resource.
					getApplicationContext(),
					" This is " + item.mTitle + "\n" + 
					"Description: " + item.mDescription + " long press to stop stalking it",
					Toast.LENGTH_LONG).show();
			return true; // We 'handled' this event.
		}

		// Long press cancels stalking of a particular player.
		@Override
		public boolean onItemLongPress(final int index, final VictimsOverlayItem item) {
			Toast.makeText(
					//TODO: Remove hardcoded debug text and replace with R.string resource.
					getApplicationContext(),
					"" + item.mTitle + "  pressed" + "\n"
							+ "Description: " + item.mDescription,
					Toast.LENGTH_LONG).show();
			
			//Call async functions to update stalked players...
			Player[] tmpPlayers = new Player[2];
			tmpPlayers[0]=currentPlayer;
			tmpPlayers[1]=new Player(item.getUserID());
			new StopStalkingAsync().execute(tmpPlayers);
			// Redraw the map.
			new GetInteractionsAsync(true).execute(currentPlayer);
			return false;
		}
	};

	/***
	 * Translates a radius in real world meters to the corresponding pixel value for
	 * the given map projection and latitude. Projection needed to adjust for appropriate zoom level 
	 * and latitude needed for a proper geodesi calculation since proportions would change 
	 * depending och latitude and the built in function is for use over the equator. Unchanged it will give a 
	 * significant error if uses in Sweden.
	 * @param meters The radius in real world meters.
	 * @param map The OSMDroid map projection to get the circle drawn
	 * @param latitude latitude of the circle. For Stalker app the centerpoint of the circle is used.
	 * @return an integer with the corresponding number of pixels for the latitude and projection.
	 */
	public static int metersToRadius(float meters, MapView map, double latitude) {
		return (int) (map.getProjection().metersToEquatorPixels(meters) * (1 / Math
				.cos(Math.toRadians(latitude))));
	}
}