/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.Window;
import android.widget.TextView;
/**
 * An activity that displays the five highest scoring players at the particular server.
 * Very basic as of version 1.0 No bells and whistles
 * @author Einar Sundgren
 * @version 1.0
 *
 */

public class HighScoreActivity extends Activity {
	protected GameServer gameServer;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_high_score);
		gameServer = ((StalkerApplication) this.getApplication()).getGameServer();
		// Generalization of the class demands parameters. I dont need any. Thats why empty string.
		new GetScoreAsync().execute("");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.high_score, menu);

		return true;
	}
	
	private class GetScoreAsync extends AsyncTask<String, String, ArrayList<Player>>{

		
		// No fuzz. Just get the scores.
		@Override
		protected ArrayList<Player> doInBackground(String... arg0) {
			return gameServer.getHighScore();
		}

		// Very rudimentry. Should be pimped.
		//TODO:Pimp high score activity
		@Override
		protected void onPostExecute(ArrayList<Player> result) {
			super.onPostExecute(result);
			TextView hsView = (TextView) findViewById(R.id.highscoretext);
			hsView.setText("High Score\n Points\tPlayer\n");
			for (int i = 0 ; i < result.size(); i++){
				hsView.append(result.get(i).getPoints() + "\t"+ result.get(i).getUserName()+ "\n");
			}
			
		}
		
		
		
		
		
	}

}
