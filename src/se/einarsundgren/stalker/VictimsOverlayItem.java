/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */

package se.einarsundgren.stalker;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;


/**
 * A class extending overlayItem to make it possible to pass GameInteraction objects as well
 * @author Einar
 *
 */
class VictimsOverlayItem extends OverlayItem {
	
	
	private int userID;
	// An overload so also a player object can be passed.
	public VictimsOverlayItem (String title, String description, GeoPoint geoPoint) {
		super (title, description, geoPoint);
	}
	
	public VictimsOverlayItem (String title, String description, GeoPoint geoPoint, int userID) {
		super (title, description, geoPoint);
		this.userID = userID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	
	
}