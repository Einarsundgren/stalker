/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;


import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

/***
 * A runnable class that publishes a notification. Adjusts itself for pre or post Jelly Bean (3.0)
 * notification/action bar style. Following class is heavily based on the code examples from
 * developer.android.com but rewritten for this purpose. 
 * Original code http://developer.android.com/guide/topics/ui/notifiers/notifications.html
 * I consider the class to be a bit unifinished. It works in a basic way but not in the exact way I want it to.
 * @author Einar Sundgren
 * @version 0.5
 *
 */
public class GamePlayNotification implements Runnable {

	private Context context;
	private String title;
	private String text;
	
		/***
		 * 
		 * @param context Application context to be sender of notification
		 * @param title Title of notification
		 * @param text Text of notification
		 * 
		 */
		public GamePlayNotification(Context context, String title, String text) {
		super();
		this.context = context;
		this.text = text;
		this.title = title;
	}

		@SuppressWarnings("unused")
		@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		public void run() {
			NotificationManager mNotificationManager = 
					(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);				    	
	    	

			if (true){
	    		NotificationCompat.Builder builder =
		    	        new NotificationCompat.Builder(context)
		    	        .setSmallIcon(R.drawable.agenda)
		    	        .setContentTitle(title)
		    	        .setContentText(text);
		    // Creates an intent for the supplied context
		    Intent resultIntent = new Intent(context, GameInteractionActivity.class);
	    	TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
	    	
	    	// Adds the back stack for the Intent (but not the Intent itself)
	    	stackBuilder.addParentStack(GameInteractionActivity.class);
	    	
	    	 // Adds the Intent that starts the Activity to the top of the activity stack
	    	  stackBuilder.addNextIntent(resultIntent);
	    	  PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
	    	  builder.setContentIntent(resultPendingIntent);
	    	  
	    	  //Flash lights. Not working in emulator.
	    	  //TODO: Check if this works on a real device
	    	  builder.setLights(Color.RED, 500, 500);
	    	  
	    	  // Vibrate
	    	//TODO: Check if this works on a real device
	    	  long[] pattern = {500,500,500,500,500,500,500,500,500};
	    	  builder.setVibrate(pattern);
	    	  
              builder.setStyle(new NotificationCompat.InboxStyle());
	    	  
              // Audible alert as well
	    	  Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	    	  builder.setSound(alarmSound);
	    	  mNotificationManager.notify(1, builder.build());
	    	} else {
	    		// If version older. Do it another way. 
	    		// But that crashed it while testing.
	    		//TODO:Work on backwards compatibility for notifications.

	    	}	    	
	    }
	 }
	
