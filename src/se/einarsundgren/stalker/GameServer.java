/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * Created by Einar Sundgren on 2013-06-13.
 * A communications channel between the client and the serer. 
 * NB that this is not the actual server, just an interface class.
 * In 1.0 this is used to communicate only with server 1.0 and compatibility 
 * with other versions could potentially give you lots of errors.
 * 
 * Communication to servers are based on HTTP posts. Returns are text strings.
 * 
 * @author Einar Sundgren
 * @version 1.0
 */
public class GameServer   {
	private static final String TAG = "GameServerActivity";

	/* Finals that denotes which type of call is made.*/
	private final String SAVE_POS = "save_pos";
	private final String GET_PLAYERS = "get_players";
	private final String DEBUG_POS = "debug_pos";
	private final String POPULATE_PLAYER = "populate_player";
	private final String GET_GAME_INTERACTIONS = "get_game_interactions";
	private final String UPDATE_SCORE = "update_score";
	private final String STALK_PLAYER = "stalk_player";
	private final String STOP_STALKING = "stop_stalking";
	private final String STALKED_WARNED = "stalked_warned";
	private final String BEEN_IN_RANGE = "been_in_range";
	private final String GET_HIGHSCORE = "get_highscore";
	private final String PLAYER_EXISTS = "player_exists";
	private final String REGISTER_PLAYER = "register_player";
	private final String ADD_FRIEND = "add_friend";
	private final String REMOVE_FRIEND = "remove_friend";
	private final String GET_FRIENDS = "get_friends";
	
	/* Finals that denotes string delimiters in returns from server.*/
	private final String PLAYER_DELIMITER = "##";
	private final String PD_DELIMITER = "#";

	/* Denotes positions of the player data in 
	 * player data string returned from server.*/
	private final int PD_USER_ID = 0;
	private final int PD_LAT = 1;
	private final int PD_LON = 2;
	private final int PD_POINTS = 3;
	private final int PD_USERNAME = 4;
	private final int PD_LAST_UPDATED = 5;
	

	/**
	 * Denotes positions of interactions data in string returned from server
	 */
	private final int ID_STALKER_ID = 0;
	private final int ID_STALKER_LAT = 1;
	private final int ID_STALKER_LON = 2;
	private final int ID_STALKER_USERNAME = 3;
	private final int ID_STALKER_POINTS = 4;
	private final int ID_STALKER_UPDATED = 5;

	// Stalked user
	private final int ID_STALKED_ID = 6;
	private final int ID_STALKED_LAT = 7;
	private final int ID_STALKED_LON = 8;
	private final int ID_STALKED_USERNAME = 9;
	private final int ID_STALKED_POINTS = 10;
	private final int ID_STALKED_UPDATED = 11;
	private final int ID_STALKED_WARNED = 12;
	private final int ID_INTERACTION_ID = 13;
	private final int ID_CREATION_TIME = 14;
	private final int ID_CURRENT_TIME = 15;
	private final int ID_BEEN_IN_RANGE = 16;

	/* Return codes from the Stalker server*/
	private final String EXPECTED_SAVE_COORDS_RETURN = "11";
	private final String RETURN_OK ="{\"operation_ok\":\"true\"}";

	/* Context passed from the calling Activity*/
	//private Context context;

	public int getNumPlayers() {
		return numPlayers;
	}

	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}

	public Double getDist() {
		return dist;
	}

	public void setDist(Double dist) {
		this.dist = dist;
	}

	/*
	 * Variables that determine the distance to other players and the maximum
	 * number of players to display. They must be set in the parent class but
	 * are accessed by the inner AsyncTask classes. Its all techincal on how its
	 * possible to pass params to AsyncTasks.
	 */
	
	protected Double dist;
	protected int numPlayers;

	/* Formatted content of http post*/
	private HttpPost httppost;
	/* Http client object*/
	private HttpClient httpClient;
	/* Response object from http Call*/
	private HttpResponse httpResponse;
	/* Stream that returns the http string*/
	private InputStream is;

	/***
	 * Constructor that takes the apps context and server URI and creates a connection to it.
	 * @param serverName
	 * @param context
	 */
	//TODO: check if the server connection is actually working. Or throw an exception.
	public GameServer(String serverName) {
		httpClient = new DefaultHttpClient();
		httppost = new HttpPost(serverName);
		//this.context = context;
	}

	/**
	 * Saves the coordinates of the current player to the database.
	 * @param currentPlayer player object to be saved.
	 * @return a string displaying possible errors. 11 is all fine. 10 or 01 or 0 are error codes.
	 */
	public String saveCoordinates(Player currentPlayer) {

		int userID = currentPlayer.getUserID();
		Log.d(TAG, "Saving with: " + currentPlayer.getUserID()
						+ currentPlayer.getUserName());
		String password = currentPlayer.getPassword();
		Double lat = currentPlayer.getLat();
		Double lon = currentPlayer.getLon();

		String returnString = null;

		try {
			/* Add user credentials and lat/lon to HTTP Post request*/
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason", SAVE_POS));
			nameValuePairs.add(new BasicNameValuePair("userID", Integer
					.toString(userID)));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			nameValuePairs.add(new BasicNameValuePair("lat", lat.toString()));
			nameValuePairs.add(new BasicNameValuePair("lon", lon.toString()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			Log.d(TAG, returnString);
			if (!returnString.equals(EXPECTED_SAVE_COORDS_RETURN)) {
				throw new IOException("Unexpected server return");
			}
		} catch (IOException e) {
			Log.d(TAG, "Async updateLocation: " + e.getMessage());
		}
		return returnString;

	}

	/**
	 * Adds a new tuple to the StalkerUserPosition table for each call. No
	 * changes are done to previous ones. All work
	 * !! As of version 1.0 of the server this should only be used for debugging and will produce 
	 * errors later on if used as the standard method for updating coordinates!!
	 * @player player
	 * @ return 11 for all fine. 10, 01 or 0 ar all error codes.
	 */
	public String debugPosition(Player player) {
		int userID = player.getUserID();
		String password = player.getPassword();
		Double lat = player.getLat();
		Double lon = player.getLon();

		try {
			/* Add user credentials and lat/lon*/
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason", DEBUG_POS));
			nameValuePairs.add(new BasicNameValuePair("userID", Integer
					.toString(userID)));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			nameValuePairs.add(new BasicNameValuePair("lat", lat.toString()));
			nameValuePairs.add(new BasicNameValuePair("lon", lon.toString()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
		} catch (IOException e) {
			Log.d(TAG, "Async debug location: " + e.getMessage());
		}

		return inputStreamToString(is).toString();
	}

	/**
	 * Lists the players free for stalking within a radius set in the server. 
	 * 
	 * @param player the player which is the center of the map. Likely to be currentPlayer
	 * @return an arraylist of player objects matching the conditions. If none found return is null.
	 */
	public ArrayList<Player> listPossibleVictims(Player player)
		
			throws Exception {
		Log.d(TAG, "Lists  victims");
		int userID = player.getUserID();
		String password = player.getPassword();
		Double lat = player.getLat();
		Double lon = player.getLon();
		ArrayList<Player> playerList = new ArrayList<Player>();
		
		/* Add user credentials and lat/lon*/
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("reason", GET_PLAYERS));
		nameValuePairs.add(new BasicNameValuePair("userID", Integer
				.toString(userID)));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("lat", lat.toString()));
		nameValuePairs.add(new BasicNameValuePair("lon", lon.toString()));
		Log.d(TAG, " Number of players" + numPlayers);
		nameValuePairs.add(new BasicNameValuePair("dist", dist.toString()));
		nameValuePairs.add(new BasicNameValuePair("numPlayers", Integer
				.toString(numPlayers)));
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		httpResponse = httpClient.execute(httppost);
		HttpEntity entity = httpResponse.getEntity();
		
		/*
		 * Puts return of call in an inputstream and converts to a proper string
		 * that is split along the "##" into a string array.
		 */
		Log.d(TAG, " Raw Return from update: " + inputStreamToString(is).toString());
		is = entity.getContent();
		String[] playerStrings = inputStreamToString(is).toString().trim().split(
				PLAYER_DELIMITER);
		Log.d(TAG, "RAW" + inputStreamToString(is).toString() + playerStrings[0]);
		if (playerStrings.length <= 1) {
			Log.d(TAG, "Too short");
			
		}
		if (playerStrings.length > 1){
		for (int i = 0; i < playerStrings.length; i++) {
			/* New objct each iteration*/
			Player tmpPlayer = new Player();
			/* Split for values*/

			String playerData[] = playerStrings[i].split("#");
			Log.d(TAG, playerData[PD_USER_ID]);
			tmpPlayer.setUserID(Integer.parseInt(playerData[PD_USER_ID]));
			tmpPlayer.setLon(Double.parseDouble(playerData[PD_LON]));
			tmpPlayer.setLat(Double.parseDouble(playerData[PD_LAT]));

			/* Add to arraylist of players to return.*/
			playerList.add(tmpPlayer);
		}
		}

		Log.d(TAG, "Result: " + inputStreamToString(is).toString());
		return playerList;
	}

	/***
	 * Gets info on a single player.
	 * @param player Player object with playername and password needed to be set.
	 * @return A player object populated with all the data 
	 */
	
	public Player getPlayerInfo(Player player) {
		String userName = player.getUserName();
		String password = player.getPassword();

		try {

			/* Add user credentials and lat/lon*/
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs
					.add(new BasicNameValuePair("reason", POPULATE_PLAYER));
			nameValuePairs.add(new BasicNameValuePair("username", userName));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);
			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			String playerString = inputStreamToString(is).toString();
			Log.d(TAG, "RAW user info: " + playerString);
			if(playerString.length()<10) return null;

			/* Split for values*/
			String playerData[] = playerString.split("#");
			Log.d(TAG, playerData.length + " " + playerData[PD_USER_ID]);
			player.setUserID(Integer.parseInt(playerData[PD_USER_ID]));
			Log.d(TAG, "Lon: " + playerData[PD_LON]);
			player.setLon(Double.parseDouble(playerData[PD_LON]));
			Log.d(TAG, "Lat: " + playerData[PD_LAT]);
			player.setLat(Double.parseDouble(playerData[PD_LAT]));
			player.setPoints(Integer.parseInt(playerData[PD_POINTS]));
			player.setUserName(playerData[PD_USERNAME]);


		} catch (Exception e) {
			Log.d(TAG, "Loading of player failed: " + e.getMessage());
			return null;
		}
		return player;
	};

	/**
	 * Lists all interaction a particular player has with the other players.
	 * @param player The player to be tested for.
	 * @return an arraylist of interaction objects.
	 * @throws Exception As thrown by HTTP request.
	 */
	
	public ArrayList<GameInteraction> getGameInteractions(Player player)
			throws Exception {
		/* Check if player object is valid. Throw exception if not.*/
		if ((player == null) || player.getUserID() <= 0
				|| player.getUserName() == null) {
			throw new Exception("Player object not valid.");
		}

		String password = player.getPassword();
		int userID = player.getUserID();

		ArrayList<GameInteraction> interactions = new ArrayList<GameInteraction>();

		/* Add user credentials to Post */
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("reason",
				GET_GAME_INTERACTIONS));
		nameValuePairs.add(new BasicNameValuePair("userID", Integer
				.toString(userID)));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		httpResponse = httpClient.execute(httppost);
		HttpEntity entity = httpResponse.getEntity();
		is = entity.getContent();
		String returnString = inputStreamToString(is).toString();
		Log.d(TAG, "Get interactions" + returnString);

		/*
		 * Puts return of call in an inputstream and converts to a proper string
		 * that is split along the "##" into a string array.
		 */

		String[] playerStrings;
		
		Log.d(TAG, "Returned string length " + returnString.length());
		if (returnString.length() > 0) {
		playerStrings = returnString.trim().split(PLAYER_DELIMITER);
		Log.d(TAG, "Returned array length " + playerStrings.length);
		} else {
			return null;

		}

		for (int i = 0; i < playerStrings.length; i++) {
			/*New object each iteration*/
			GameInteraction tmpInteraction = new GameInteraction();
			Player stalker = new Player();
			Player stalked = new Player();

			/* Split for values*/
			String interactionData[] = playerStrings[i].split(PD_DELIMITER);
			Log.d(TAG, interactionData[PD_USER_ID]);
			/* Create stalker part*/
			stalker.setUserID(Integer.parseInt(interactionData[ID_STALKER_ID]));
			stalker.setLat(Double.parseDouble(interactionData[ID_STALKER_LAT]));
			stalker.setLon(Double.parseDouble(interactionData[ID_STALKER_LON]));
			stalker.setUserName(interactionData[ID_STALKER_USERNAME]);
			stalker.setPoints(Integer
					.parseInt(interactionData[ID_STALKER_POINTS]));
			//stalker.setLastUpdated(Integer.parseInt(interactionData[ID_STALKER_UPDATED]));

			stalked.setUserID(Integer.parseInt(interactionData[ID_STALKED_ID]));
			stalked.setLat(Double.parseDouble(interactionData[ID_STALKED_LAT]));
			stalked.setLon(Double.parseDouble(interactionData[ID_STALKED_LON]));
			stalked.setUserName(interactionData[ID_STALKED_USERNAME]);
			stalked.setPoints(Integer
					.parseInt(interactionData[ID_STALKED_POINTS]));
			// stalked.setLastUpdated(Integer.parseInt(interactionData[ID_STALKED_UPDATED]));
			
			
			Log.d(TAG, "Warned:"+  interactionData[ID_STALKED_WARNED]);
			
			if(interactionData[ID_STALKED_WARNED].trim().equals("0")) tmpInteraction.setStalkedWarned(false);
			else tmpInteraction.setStalkedWarned(true);
			
			if(interactionData[ID_BEEN_IN_RANGE].trim().equals("0")) tmpInteraction.setHasBeenInRange(false);
			else tmpInteraction.setHasBeenInRange(true);
			
			tmpInteraction.setStartTime(Long.parseLong(interactionData[ID_CREATION_TIME]));
			tmpInteraction.setCurrentServerTime(Long.parseLong(interactionData[ID_CURRENT_TIME]));
			 
			tmpInteraction.setInteractionID(Integer.parseInt(interactionData[ID_INTERACTION_ID]));
			/* Add newly populated player objects to interactions object.*/
			tmpInteraction.setStalker(stalker);

			tmpInteraction.setStalked(stalked);
			Log.d(TAG, "Stalker: " + stalker.getUserName() + " @ " + stalker.getLat() + ", " + stalker.getLon());
			Log.d(TAG, "Stalked: " + stalked.getUserName() + " @ " + stalked.getLat() + ", " + stalked.getLon());

			/* Add interactions object to list of interactions*/
			interactions.add(tmpInteraction);
		}

		/* Return interactions list*/
		return interactions;
	}
	
	/**
	 * Changes the score for the supplied combo of player ID and password.
	 * @param player Player object with id and password set.
	 * @param difference The difference in score as a int.
	 * @return the new score of the player.
	 */
	public int changeScore(Player player, int difference){
		int userID = player.getUserID();
		Log.d(TAG, "Saving with id: " + player.getUserID()
						+ " player: " + player.getUserName() + " diff: " + Integer.toString(difference));
		
		String password = player.getPassword();

		String returnString = null;

		try {
			/* Add user credentials and lat/lon*/
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason", UPDATE_SCORE));
			nameValuePairs.add(new BasicNameValuePair("userID", Integer.toString(userID)));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			nameValuePairs.add(new BasicNameValuePair("difference", Integer.toString(difference)));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			Log.d(TAG, "Updating score: " + returnString);

		} catch (IOException e) {
			Log.d(TAG, "Async updateLocation: " + e.getMessage());
		}
		int newScore = 0;
		
		try {
			Integer.parseInt(returnString);
			
		}catch (NumberFormatException e){
			Log.d(TAG, "Number format for update: " + e.getMessage() + " " + returnString);
		}
		
		return newScore;
		
	}
	
	/**
	 * Start a stalking interaction with another player.
	 * @param stalker The stalking player calling this method.
	 * @param stalked The player to be stalked.
	 */
	
	public void stalkPlayer(Player stalker, Player stalked){
		Log.d(TAG, "Calling game server Player: " +  stalker.getUserName()+ " Should stalk " + stalked.getUserID());
		
		int userID = stalker.getUserID();
		String password = stalker.getPassword();
		String returnString = null;

		try {
			/* Add user credentials and lat/lon*/
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason", STALK_PLAYER));
			nameValuePairs.add(new BasicNameValuePair("userID", Integer.toString(userID)));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			nameValuePairs.add(new BasicNameValuePair("stalkedID", Integer.toString(stalked.getUserID())));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			Log.d(TAG, "Stalking operation updated: " + returnString);

		} catch (IOException e) {
			Log.d(TAG, "Async start stalking: " + e.getMessage());
		}
					
	};
	
	/**
	 * Interrupt stalking interaction
	 * @param stalker the currently stalking player
	 * @param stalked the player that is stalked.
	 */
	
	public void stopStalking(Player stalker, Player stalked){
		Log.d(TAG, "Calling game server Player: " +  stalker.getUserName()+ " Should STOP stalking of " + stalked.getUserID());
		
		int userID = stalker.getUserID();
		String password = stalker.getPassword();
		String returnString = null;

		try {
			// Add user credentials and lat/lon
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason", STOP_STALKING));
			nameValuePairs.add(new BasicNameValuePair("userID", Integer.toString(userID)));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			nameValuePairs.add(new BasicNameValuePair("stalkedID", Integer.toString(stalked.getUserID())));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			Log.d(TAG, "Stop stalking operation updated: " + returnString);
			
		} catch (IOException e) {
			Log.d(TAG, "Async stop stalking: " + e.getMessage());
		}
		
	}
	
	/**
	 * Flag that a stalked player has been warned of the interaction.
	 * @param gameInteraction Interaction to be changed.
	 */
	public void setStalkedWarned(GameInteraction gameInteraction){
		Log.d(TAG, "Setting stalked warned ");
		int interactionID = gameInteraction.getInteractionID();
		String returnString = null;
		gameInteraction.setStalkedWarned(true);

		try {
			// Add user credentials and lat/lon
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason", STALKED_WARNED));
			nameValuePairs.add(new BasicNameValuePair("interactionID", Integer.toString(interactionID)));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			Log.d(TAG, "Stalked warned operation updated: " + returnString);

		} catch (IOException e) {
			Log.d(TAG, "Stalked warned: " + e.getMessage());
		}
		
		
	}
	
	/**
	 * Sets the flag that the stalker has been in a point giving range of the stalked player.
	 * @param gameInteraction
	 */
	
	public void setHasBeenInRange(GameInteraction gameInteraction){
		Log.d(TAG, "Setting has been in range ");
		int interactionID = gameInteraction.getInteractionID();
		String returnString = null;
		gameInteraction.setHasBeenInRange(true);

		try {
			// Add user credentials and lat/lon
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason", BEEN_IN_RANGE));
			nameValuePairs.add(new BasicNameValuePair("interactionID", Integer.toString(interactionID)));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			Log.d(TAG, "Stalked warned operation updated: " + returnString);

		} catch (IOException e) {
			Log.d(TAG, "Stalked warned: " + e.getMessage());
		}
		
		
	}
	/**
	 * Just get the highscore of the players of that server.
	 * @return Arraylist of players with their scores set.
	 */
	public ArrayList<Player> getHighScore(){
		Log.d(TAG, "Setting has been in range ");
		ArrayList<Player> returnPlayers = new ArrayList<Player>();
		try {
			// Add user credentials and lat/lon
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason", GET_HIGHSCORE));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			String returnString = inputStreamToString(is).toString();
			Log.d(TAG, "Retreived highscore: " + returnString);
			
			
			
			String[] records = returnString.split("##");
			for (int i = 0; i<records.length; i ++){
				String [] temp = records[i].split("#");
				returnPlayers.add(new Player(temp[0], Integer.parseInt(temp[1])));
			}

		} catch (IOException e) {
			Log.d(TAG, "Retreieved highscore: " + e.getMessage());
		}
		return returnPlayers;
	}
	
	/**
	 * Adds new player to the server
	 * @param player The player data that the user wants to register.
	 * @throws Exception
	 */
	public boolean registerNewPlayer(String name, String password, String email) throws Exception{
		String returnString;
		try {
			// Add user credentials and lat/lon
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason",REGISTER_PLAYER ));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			nameValuePairs.add(new BasicNameValuePair("name", name));
			nameValuePairs.add(new BasicNameValuePair("email", email));
			Log.d(TAG, "PW: " + password+ " Name: " + name + " Mail: "+ email);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			Log.d(TAG, "Register new player result: " + returnString);
			if (returnString.equals("{\"newPlayerRegistered\":\"true\"}")) 
				{
				Log.d(TAG, "All fine. Returning true.");
				return true;}
			else return false;

		} catch (IOException e) {
			Log.d(TAG, "Stalked warned: " + e.getMessage());
			return false;
		}
		
		
		
	}
	
	public boolean playerNameExists(String name){
			String returnString = "";
			Log.d(TAG, "Testing if Player exists: " + name);
		try {
			// Add user credentials and lat/lon
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("reason", PLAYER_EXISTS));
			nameValuePairs.add(new BasicNameValuePair("name", name));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			Log.d(TAG, "Player exists returned: " + returnString);

		} catch (IOException e) {
			Log.d(TAG, "Player exists: " + e.getMessage());
		}
		if (returnString.equals("{\"nameExists\":\"true\"}")){
			return true;
		}else {
			return false;
		}
	}
	
	public boolean addFriend(int user1ID, int user2ID){
		
		String jsonCommand;
		String returnString = null;
		
		jsonCommand="{\"command\":\""+ADD_FRIEND+"\", \"user1id\":\"" +user1ID+"\", \"user2id\":\"" +user2ID+"\"}";

		try {
			/* Add user credentials and lat/lon to HTTP Post request*/
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("json_data", jsonCommand));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			Log.d(TAG, returnString);
			if (!returnString.equals(RETURN_OK)) {
				return false;			}
		} catch (IOException e) {
			Log.d(TAG, "Async updateLocation: " + e.getMessage());
			return false;
		}
		return true;
		
	}
	
	public ArrayList<Player> getFriends(Player player){
		String jsonCommand;
		String returnString = null;
		ArrayList<Player> returnList;
		
		jsonCommand="{\"command\":\""+GET_FRIENDS+"\", \"user_id\":\"" +player.getUserID()+"\"}";

		try {

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("json_data", jsonCommand));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httppost);

			HttpEntity entity = httpResponse.getEntity();
			is = entity.getContent();
			returnString = inputStreamToString(is).toString();
			//Log.d(TAG,"Server: " + returnString);
			
			if (returnString.equals(RETURN_OK)) {
				return null;			}
		} catch (IOException e) {
			Log.d(TAG, "Async updateLocation: " + e.getMessage());
			return null;
		}
		/*
		returnString = ""
				+ "{\"friends\": ["
				+ "{ \"name\":\"John\" , \"user_id\":5, \"lat\":\"5.3\", \"lon\":\"10.3\",\"points\":5332,\"updated\":\"2012-04-23T18:25:43.511Z\" },"
				+ "{ \"name\":\"Erik\" , \"user_id\":6, \"lat\":\"5.3\", \"lon\":\"10.3\",\"points\":5332,\"updated\":\"2012-04-23T18:25:43.511Z\" },"
				+ "{ \"name\":\"Per\" , \"user_id\":7, \"lat\":\"5.3\", \"lon\":\"10.3\",\"points\":5332,\"updated\":\"2012-04-23T18:25:43.511Z\" },"
				+ "{ \"name\":\"Tor\" , \"user_id\":8, \"lat\":\"5.3\", \"lon\":\"10.3\",\"points\":5332,\"updated\":\"2012-04-23T18:25:43.511Z\" },"
				+ "{ \"name\":\"Mark\" , \"user_id\":9, \"lat\":\"5.3\", \"lon\":\"10.3\",\"points\":5332,\"updated\":\"2012-04-23T18:25:43.511Z\" },"
				+ "{ \"name\":\"Finn\" , \"user_id\":10, \"lat\":\"5.3\", \"lon\":\"10.3\",\"points\":5332,\"updated\":\"2012-04-23T18:25:43.511Z\" },"
				+ "{ \"name\":\"Ernst\" , \"user_id\":56, \"lat\":\"5.3\", \"lon\":\"10.3\",\"points\":5332,\"updated\":\"2012-04-23T18:25:43.511Z\" },"
				+ "{ \"name\":\"Kurt\" , \"user_id\":123, \"lat\":\"5.3\", \"lon\":\"10.3\",\"points\":5332,\"updated\":\"2012-04-23T18:25:43.511Z\" }"
				+ "]}";
		*/
		
		try {
			Log.d(TAG, returnString);
			JSONObject returObject = new JSONObject(returnString);
			JSONArray retur = (JSONArray) returObject.get("friends");
			JSONObject obj;
			returnList=new ArrayList<Player>();
			for (int i = 0; i<retur.length(); i++){
			obj = retur.getJSONObject(i);
			Player tmp = new Player();
			tmp.setUserID(obj.getInt("user_id"));
			tmp.setUserName(obj.getString("name") );
			tmp.setLat(Double.parseDouble(obj.getString("lat") ));
			tmp.setLon(Double.parseDouble(obj.getString("lon") ));
			tmp.setPoints(obj.getInt("points"));
			returnList.add(tmp);

			Log.d("JSON","Name: " + obj.getString("name") + "ID: " + obj.getInt("user_id") + " Lat: " + obj.getString("lat") + " Lon: " + obj.getString("lon") 
					+ " Update: " + obj.getString("updated") + " Points: " + obj.getString("points") );
			
			
			}
			return returnList;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.d(TAG,e.getMessage());
		}
		return null;
	}
	
	

	/**
	 * 	Method to convert the inputstream from a HttpResponse to a string.
	 * @param is inputstream object
	 * @return string object
	 */

	public static StringBuilder inputStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();

		// Wrap a BufferedReader around the InputStream
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));

		// Read response until the end
		try {
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Return full string
		return total;
	}

}