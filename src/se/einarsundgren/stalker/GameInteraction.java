/**
 *  Copyright (C) 2013 Einar Sundgren www.einarsundgren.se/stalker
 *
 * This file is part of Stalker.
 *
 * Stalker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Stalker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Stalker.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.einarsundgren.stalker;


/**
 * This class models an interaction between two players.
 * It has two player objects as either stalker or stalked. 
 * Created by Einar on 2013-06-27.
 * @author Einar Sundgren
 * @version 1.0
 */
public class GameInteraction {
	/**
	 * The central Player objects stalker and stalked
	 */
	private Player stalker;
	private Player stalked;
	/**
	 * Flags if the staled has been notified about the interactions.
	 */
	private boolean stalkedWarned;
	/**
	 * Id of the interaction. Not used in v 1.x of the server.
	 */
	private int interactionID;
	/**
	 * Flags if the stalker has been in a range less than StalkerApplication.OUT_OF_RANGE of the stalked
	 * and thus has been given any points.
	 */
	private boolean hasBeenInRange;
	/**
	 * Timestamp when the interaction started according to server clock in seconds.
	 */
	private long startTime;
	/**
	 * current server time in seconds.
	 */
	private long currentServerTime;
	/**
	 * current servertime - startTime;
	 */
	private long timeDiff;


	
/**
 * Calculated the distance between the stalker and the stalked using the haversin formula and the player coordinates.
 * For the thing to work there must be two valid player objects with coordinates. 
 * Otherwise you risk a null pointer exception
 * 
 * @return the distance between the player objects stalker and stalked 
 */
	public Double getDistance() {
		Double distance = haversin(stalked.getLat(), stalked.getLon(),
				stalker.getLat(), stalker.getLon());
		return distance;
	}

	public long getCurrentServerTime() {
		return currentServerTime;
	}

	public void setCurrentServerTime(long currentServerTime) {
		this.currentServerTime = currentServerTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
		this.timeDiff = startTime - (System.currentTimeMillis()/1000);
	}

	public long getTimeDiff() {
		return timeDiff;
	}


	public boolean hasBeenInRange() {
		return hasBeenInRange;
	}

	public void setHasBeenInRange(boolean hasBeenInRange) {
		this.hasBeenInRange = hasBeenInRange;
	}

	/**
	 * Constructor using two player objects, flags if stalker is warned, interactionID and starttime.
	 * @param stalker
	 * @param stalked
	 * @param stalkedWarned
	 * @param interactionID
	 * @param startTime
	 */
	public GameInteraction(Player stalker, Player stalked,
			boolean stalkedWarned, int interactionID, Long startTime) {
		super();
		this.stalker = stalker;
		this.stalked = stalked;
		this.stalkedWarned = stalkedWarned;
		this.interactionID = interactionID;
		this.startTime = startTime;
	}
	/**
	 * Default constructor. Avoid using it. Should only be used to create empty objects 
	 * and could result in nullPointerException. No other use.
	 */
	public GameInteraction() {
		super();
		//this.startTime = new Time();
		//startTime.setToNow();
	}

	public Player getStalker() {
		return stalker;
	}

	public void setStalker(Player stalker) {
		this.stalker = stalker;
	}

	public Player getStalked() {
		return stalked;
	}
	
	/**
	 * Returns how long the interaction has been going on in seconds.
	 * @return age of interaction in seconds.
	 */
	public long getAge(){
		// Return difference between starttime and now.
		return (this.currentServerTime-this.startTime) ;
	}

	public void setStalked(Player stalked) {
		this.stalked = stalked;
	}
	
	public boolean isStalkedWarned() {
		return stalkedWarned;
	}

	public void setStalkedWarned(boolean stalkedWarned) {
		this.stalkedWarned = stalkedWarned;
	}

	public int getInteractionID() {
		return interactionID;
	}

	public void setInteractionID(int interactionID) {
		this.interactionID = interactionID;
	}

	/**
	 * Implementation of the Haversin formula for calculating the distance
	 * between two points on a sphere.
	 * 
	 * @param startLat starting latitude
	 * @param startLon staring longitude
	 * @param endLat end latitude 
	 * @param endLon end longitude
	 * @return distance in km.
	 */
	public static Double haversin(Double startLat, Double startLon,
			Double endLat, Double endLon) {

		final int r = 6371; // Radius of the earth i km. Decides what unit the return is in. (km * 1000 = meters) 
		/*
		 * startLat = 57.69208512; startLon = 11.92348234; endLat = 57.688991;
		 * endLon = 11.920123;
		 * 
		 * Debug distances should give 0.3978 km.
		 */

		Double latDistance = toRad(endLat - startLat);
		Double lonDistance = toRad(endLon - startLon);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(toRad(startLat)) * Math.cos(toRad(endLat))
				* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		Double distance = r * c;
		
		//Return distance between two points in km
		return distance;
	}
/**
 * Changes the input value in degrees to radians
 * @param value
 * @return
 */
	private static double toRad(double value) {
		return value * Math.PI / 180;
	}
}
